#include <QApplication>
#include <QDebug>

#include "mainwindow.h"
#include "predicates.h"

int main(int argc, char *argv[])
{
    predicates::init();

	QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("Winnies");
    QCoreApplication::setOrganizationDomain("gigue.winnies.org");
    QCoreApplication::setApplicationName("The Demolisher");
    QIcon::setFallbackThemeName("breeze");

	MainWindow w;
	w.show();

    return a.exec();
}

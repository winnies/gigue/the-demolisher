#include "objectmodel.h"
#include "datastream.h"

ObjectModel::ObjectModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int ObjectModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_objects.size();
}

QVariant ObjectModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto& o = m_objects[index.row()];
    switch (role) {
    case Qt::DisplayRole:
        return o->name;
    default:
        return QVariant();
    }
}

const QString &ObjectModel::getName(int index) const
{
    return m_objects[index]->name;
}

void ObjectModel::setName(int index, const QString &name)
{
    m_objects[index]->name = name;
    auto i = createIndex(index, 0);
    emit dataChanged(i, i, {Qt::DisplayRole});
}

const Object &ObjectModel::get(int index) const
{
    return *m_objects[index];
}

Object &ObjectModel::get(int index)
{
    return *m_objects[index];
}

void ObjectModel::insert(ObjectPtr object)
{
    beginInsertRows(QModelIndex(), m_objects.size(), m_objects.size());
    m_objects.push_back(std::move(object));
    endInsertRows();
}

void ObjectModel::remove(int index)
{
    beginRemoveRows(QModelIndex(), index, index);
    m_objects.erase(m_objects.begin() + index);
    endRemoveRows();
}

void ObjectModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, m_objects.size()-1);
    m_objects.clear();
    endRemoveRows();
}

QDataStream &operator<<(QDataStream &s, const ObjectModel &m)
{
    s << (quintptr)m.m_objects.size();
    for (const auto& o : m.m_objects) {
      s << o;
    }
    return s;
}

QDataStream &operator>>(QDataStream &s, ObjectModel &m)
{
    quintptr nbObjects;
    s >> nbObjects;
    m.clear();
    m.beginInsertRows(QModelIndex(), 0, nbObjects-1);
    m.m_objects.reserve(nbObjects);
    for (size_t i = 0; i < nbObjects; ++i) {
        ObjectPtr ptr;
        s >> ptr;
        m.m_objects.push_back(std::move(ptr));
    }
    m.endInsertRows();
    return s;
}

ObjectTypeFilterModel::ObjectTypeFilterModel(Object::Type wantedType, QObject *parent)
    : QSortFilterProxyModel(parent)
    , wantedType(wantedType)
{}

bool ObjectTypeFilterModel::filterAcceptsRow(int source_row, const QModelIndex &) const {
    auto model = dynamic_cast<ObjectModel*>(sourceModel());
    auto object = &model->get(source_row);
    return object->type == wantedType;
}

bool ObjectTypeFilterModel::filterAcceptsColumn(int, const QModelIndex &) const {
    return true;
}

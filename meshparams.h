#ifndef MESHPARAMS_H
#define MESHPARAMS_H

#include "params.h"
#include "myopenglwidget.h"
#include "object.h"

namespace Ui {
class MeshParams;
}

class MeshParams : public Params
{
    Q_OBJECT

public:
    explicit MeshParams(myOpenGLWidget* view, MeshObject* mesh, QWidget *parent = nullptr);
    ~MeshParams();

    void centerObject();

protected:
    void write(QDataStream&) const;
    void read(QDataStream&);

private slots:
    void on_saveAs_clicked();

private:
    void loadMesh();
    void updateView();

    void flatMode();
    void smoothMode();
    void wiredMode();

private:
    Ui::MeshParams *ui;
    myOpenGLWidget* view;
    MeshObject* mesh;

    DrawId meshDI = 0;

};

#endif // MESHPARAMS_H

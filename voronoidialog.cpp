#include "voronoidialog.h"
#include "ui_voronoidialog.h"
#include "objectmodel.h"
#include "algo.h"

VoronoiDialog::VoronoiDialog(ObjectModel *objectModel, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VoronoiDialog),
    objectModel(objectModel)
{
    ui->setupUi(this);

    meshFilter = new ObjectTypeFilterModel(Object::Type::Mesh, this);
    meshFilter->setSourceModel(objectModel);
    pointsFilter = new ObjectTypeFilterModel(Object::Type::Points, this);
    pointsFilter->setSourceModel(objectModel);
    triFilter = new ObjectTypeFilterModel(Object::Type::Triangulation, this);
    triFilter->setSourceModel(objectModel);

    ui->inputMesh->setModel(meshFilter);
    ui->inputCells->setModel(pointsFilter);
    ui->outputMesh->setModel(meshFilter);
    ui->outputTriangulation->setModel(triFilter);
    ui->tempCells->setModel(meshFilter);
}

VoronoiDialog::~VoronoiDialog()
{
    delete ui;
}

void VoronoiDialog::on_runSteps_clicked()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    auto inputMeshIdx = meshFilter->mapToSource(meshFilter->index(ui->inputMesh->currentIndex(), 0));
    auto inputCellsIdx = pointsFilter->mapToSource(pointsFilter->index(ui->inputCells->currentIndex(), 0));
    auto outputMeshIdx = meshFilter->mapToSource(meshFilter->index(ui->outputMesh->currentIndex(), 0));
    auto outputTriIdx = triFilter->mapToSource(triFilter->index(ui->outputTriangulation->currentIndex(), 0));
    auto tempCellsIdx = meshFilter->mapToSource(meshFilter->index(ui->tempCells->currentIndex(), 0));

    auto needInputMesh = ui->doConvexHull->isChecked() || ui->doIntersection->isChecked();
    auto needSeeds = ui->doDelaunay->isChecked();
    auto needOutputMesh = ui->doIntersection->isChecked();
    auto needTri = ui->doConvexHull->isChecked() || ui->doDelaunay->isChecked() || ui->doDual->isChecked();
    auto needCells = ui->doDual->isChecked() || ui->doIntersection->isChecked();

    if ((needInputMesh && !inputMeshIdx.isValid()) ||
        (needSeeds && !inputCellsIdx.isValid()) ||
        (needOutputMesh && !outputMeshIdx.isValid()) ||
        (needTri && !outputTriIdx.isValid()) ||
        (needCells && !tempCellsIdx.isValid())) return;

    auto inputMesh = needInputMesh ? &dynamic_cast<MeshObject&>(objectModel->get(inputMeshIdx.row())).mesh : nullptr;
    auto seeds = needSeeds ? &dynamic_cast<PointsObject&>(objectModel->get(inputCellsIdx.row())).points : nullptr;
    auto outputMeshObj = needOutputMesh ? &dynamic_cast<MeshObject&>(objectModel->get(outputMeshIdx.row())) : nullptr;
    auto triObj = needTri ? &dynamic_cast<TriangulationObject&>(objectModel->get(outputTriIdx.row())) : nullptr;
    auto cellsObj = needCells ? &dynamic_cast<MeshObject&>(objectModel->get(tempCellsIdx.row())) : nullptr;

    if (ui->doConvexHull->isChecked()) {
       triObj->tri = algo::incorporateInTetraTri(*inputMesh);
    }

    if (ui->doDelaunay->isChecked()) {
        algo::delaunay(*seeds, triObj->tri);
    }

    if (ui->doDual->isChecked()) {
        algo::dual(triObj->tri, cellsObj->mesh);
    }

    if (ui->doIntersection->isChecked()) {
        algo::intersection(*inputMesh, cellsObj->mesh, outputMeshObj->mesh);
    }

    if (needOutputMesh) {
        outputMeshObj->mesh.update_face_normals();
        outputMeshObj->mesh.update_vertex_normals();
        outputMeshObj->mesh.reset_colors();
        emit outputMeshObj->dataChanged();
    }

    if (needTri) {
        emit triObj->dataChanged();
    }

    if (needCells) {
        cellsObj->mesh.update_face_normals();
        cellsObj->mesh.update_vertex_normals();
        cellsObj->mesh.reset_colors();
        emit cellsObj->dataChanged();
    }
    QApplication::restoreOverrideCursor();
}

QDataStream &operator<<(QDataStream &s, const VoronoiDialog &d) {
    auto ui = d.ui;
    s << d.isVisible();
    s << d.geometry();
    for (auto * combobox : {ui->inputMesh, ui->inputCells, ui->outputMesh, ui->outputTriangulation, ui->tempCells}) {
        s << combobox->currentIndex();
    }
    for (auto * checkbox : {ui->doConvexHull, ui->doDelaunay, ui->doDual, ui->doIntersection}) {
        s << checkbox->isChecked();
    }
    return s;
}

QDataStream &operator>>(QDataStream &s, VoronoiDialog &d) {
    auto ui = d.ui;
    {
        bool tmp;
        s >> tmp; d.setVisible(tmp);
    }
    {
        QRect tmp;
        s >> tmp; d.setGeometry(tmp);
    }
    {
        int tmp;
        for (auto * combobox : {ui->inputMesh, ui->inputCells, ui->outputMesh, ui->outputTriangulation, ui->tempCells}) {
            s >> tmp; combobox->setCurrentIndex(tmp);
        }
    }
    {
        bool tmp;
        for (auto * checkbox : {ui->doConvexHull, ui->doDelaunay, ui->doDual, ui->doIntersection}) {
            s >> tmp; checkbox->setChecked(tmp);
        }
    }
    return s;
}

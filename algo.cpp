#include "algo.h"
#include "predicates.h"

#include <QMatrix4x4>
#include <QVector3D>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/convex_hull_3.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel  K;
typedef K::Point_3                                Point_3;
typedef CGAL::Surface_mesh<Point_3>               Surface_mesh;

Mesh algo::incorporateInTetra(const Mesh &mesh, bool withBaseMesh)
{

    typedef Vec3f Point;
    auto convertToQvector = [](const Point &p) {
        return QVector3D(p[0], p[1], p[2]);
    };
    auto convertToPoint = [](const QVector3D &p) {
        return Point({p.x(), p.y(), p.z()});
    };

    QVector3D barycentre = convertToQvector(mesh.calc_barycenter());
    QVector3D topTetra{0, 0, 0};
    float rayonSphere = 0.0;

    for (auto it_v = mesh.vertices_begin(); it_v != mesh.vertices_end(); it_v++)
    {
        auto point = convertToQvector(mesh.point(*it_v));
        float temp = barycentre.distanceToPoint(point);
        if (rayonSphere < temp)
        {
            rayonSphere = temp;
            topTetra = point;
        }
    }

    QVector3D topVector = topTetra - barycentre;
    topTetra += (topVector * 4);

    QVector3D perpendicularVector = QVector3D::crossProduct(topVector, QVector3D{topVector[0] + 1, topVector[1] + 1, topVector[2]});
    perpendicularVector.normalize();
    perpendicularVector *= (rayonSphere * 4);
    perpendicularVector += (topVector * -1) * 4;

    QMatrix4x4 matrice;
    QVector3D firstVertexOnSphere = perpendicularVector;

    matrice.rotate(120, topVector);
    QVector3D secondVertexOnSphere = firstVertexOnSphere;
    secondVertexOnSphere = matrice.mapVector(secondVertexOnSphere);

    matrice.rotate(120, topVector);
    QVector3D thirdVertexOnSphere = firstVertexOnSphere;
    thirdVertexOnSphere = matrice.mapVector(thirdVertexOnSphere);

    firstVertexOnSphere += barycentre;
    secondVertexOnSphere += barycentre;
    thirdVertexOnSphere += barycentre;

    Mesh tetra;
    if (withBaseMesh)
        tetra = mesh;

    Mesh::VertexHandle vhandle[4];

    vhandle[0] = tetra.add_vertex(convertToPoint(topTetra));
    vhandle[1] = tetra.add_vertex(convertToPoint(firstVertexOnSphere));
    vhandle[2] = tetra.add_vertex(convertToPoint(secondVertexOnSphere));
    vhandle[3] = tetra.add_vertex(convertToPoint(thirdVertexOnSphere));
    tetra.set_color(vhandle[0], {0, 1, 0});
    tetra.set_color(vhandle[1], {1, 0, 0});
    tetra.set_color(vhandle[2], {1, 0, 0});
    tetra.set_color(vhandle[3], {1, 0, 0});

    Mesh::FaceHandle fhandle[4];

    fhandle[0] = tetra.add_face(vhandle[0], vhandle[2], vhandle[1]);
    fhandle[1] = tetra.add_face(vhandle[2], vhandle[0], vhandle[3]);
    fhandle[2] = tetra.add_face(vhandle[3], vhandle[0], vhandle[1]);
    fhandle[3] = tetra.add_face(vhandle[1], vhandle[2], vhandle[3]);
    tetra.set_color(fhandle[0], {0.58, 0.58, 0.58});
    tetra.set_color(fhandle[1], {0.58, 0.58, 0.58});
    tetra.set_color(fhandle[2], {0.58, 0.58, 0.58});
    tetra.set_color(fhandle[3], {0.0, 0.58, 0.58});

    return tetra;
}

Triangulation algo::incorporateInTetraTri(const Mesh &mesh)
{

    typedef Vec3f Point;
    typedef Vec3f Position;

    auto convertToQvector = [](const Point &p) {
        return QVector3D(p[0], p[1], p[2]);
    };
    auto convertToPosition = [](const QVector3D &p) {
        return Position({p.x(), p.y(), p.z()});
    };

    QVector3D barycentre = convertToQvector(mesh.calc_barycenter());
    QVector3D topTetra{0, 0, 0};
    float rayonSphere = 0.0;

    for (auto it_v = mesh.vertices_begin(); it_v != mesh.vertices_end(); it_v++)
    {
        auto point = convertToQvector(mesh.point(*it_v));
        float temp = barycentre.distanceToPoint(point);
        if (rayonSphere < temp)
        {
            rayonSphere = temp;
            topTetra = point;
        }
    }

    QVector3D topVector = topTetra - barycentre;
    topTetra += (topVector * 4);

    QVector3D perpendicularVector = QVector3D::crossProduct(topVector, QVector3D{topVector[0] + 1, topVector[1] + 1, topVector[2]});
    perpendicularVector.normalize();
    perpendicularVector *= (rayonSphere * 4);
    perpendicularVector += (topVector * -1) * 4;

    QMatrix4x4 matrice;
    QVector3D firstVertexOnSphere = perpendicularVector;

    matrice.rotate(120, topVector);
    QVector3D secondVertexOnSphere = firstVertexOnSphere;
    secondVertexOnSphere = matrice.mapVector(secondVertexOnSphere);

    matrice.rotate(120, topVector);
    QVector3D thirdVertexOnSphere = firstVertexOnSphere;
    thirdVertexOnSphere = matrice.mapVector(thirdVertexOnSphere);

    firstVertexOnSphere += barycentre;
    secondVertexOnSphere += barycentre;
    thirdVertexOnSphere += barycentre;

    auto first = convertToPosition(firstVertexOnSphere);
    auto second = convertToPosition(secondVertexOnSphere);
    auto third = convertToPosition(thirdVertexOnSphere);
    auto last = convertToPosition(topTetra);

    std::array<Position, 4> vertices{first, second, third, last};

    return Triangulation(vertices);
}

void convertSurfaceMeshToMesh(const Surface_mesh &source, Mesh &target);

Mesh convexHullCGAL(const std::vector<Vec3f>& points)
{
    std::vector<Point_3> cgalpts;
    for (const auto& pt : points)
    {
        cgalpts.push_back(Point_3{pt[0], pt[1], pt[2]});
    }

    Surface_mesh sm;
    CGAL::convex_hull_3(cgalpts.begin(), cgalpts.end(), sm);

    Mesh result;
    convertSurfaceMeshToMesh(sm, result);
    return result;
}

enum class ContainsRes
{
    Inside,
    Outside,
    OnEdge,
    OnCorner
};

// from https://math.stackexchange.com/questions/544946/determine-if-projection-of-3d-point-onto-plane-is-within-a-triangle
ContainsRes pointInTriangle(const Vec3f& query_point,
                            const Vec3f& triangle_vertex_0,
                            const Vec3f& triangle_vertex_1,
                            const Vec3f& triangle_vertex_2,
                            int* vertex_opposite_query_point = nullptr) {
    // u=P2−P1
    auto u = triangle_vertex_1 - triangle_vertex_0;
    // v=P3−P1
    auto v = triangle_vertex_2 - triangle_vertex_0;
    // n=u×v
    auto n = u.cross(v);
    // w=P−P1
    auto w = query_point - triangle_vertex_0;
    // Barycentric coordinates of the projection P′of P onto T:
    // γ=[(u×w)⋅n]/n²
    auto gamma = u.cross(w).dot(n) / n.dot(n);
    // β=[(w×v)⋅n]/n²
    auto beta  = w.cross(v).dot(n) / n.dot(n);
    auto alpha = 1 - gamma - beta;
    // The point P′ lies inside T if:

    if (vertex_opposite_query_point != nullptr) {
        *vertex_opposite_query_point = -1;
        if (alpha < 0 || qFuzzyIsNull(alpha)) {
            *vertex_opposite_query_point = 0;
        } else if (beta < 0 || qFuzzyIsNull(beta)) {
            *vertex_opposite_query_point = 1;
        } else if (gamma < 0 || qFuzzyIsNull(gamma)) {
            *vertex_opposite_query_point = 2;
        }
    }

    bool is_inside = (0 <= alpha) && (alpha <= 1) && (0 <= beta) && (beta <= 1) && (0 <= gamma) && (gamma <= 1);
    if (is_inside) {
        if (qFuzzyCompare(alpha, 1) || qFuzzyCompare(beta, 1) || qFuzzyCompare(gamma,1)) {
            return ContainsRes::OnCorner;
        }
        if (qFuzzyIsNull(alpha) || qFuzzyIsNull(beta) || qFuzzyIsNull(gamma)) {
            return ContainsRes::OnEdge;
        }
        return ContainsRes::Inside;
    } else {
        return ContainsRes::Outside;
    }
}

int flip_case(const Vec3f &a, const Vec3f &b, const Vec3f &c, const Vec3f &d, const Vec3f &p, int* vertex_opposite_to_leaning_side = nullptr)
{
    // from https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection
    auto n = (b  - a).cross(c - a).normalized();
    auto l = (d - p).normalized();
    auto l_dot_n = l.dot(n);
    assert(l_dot_n != 0);
    auto inter_d = (a - p).dot(n) / l_dot_n;
    auto inter = p + l * inter_d;

    switch (pointInTriangle(inter, a, b, c, vertex_opposite_to_leaning_side))
    {
    case ContainsRes::Inside:
        return 1;
    case ContainsRes::Outside:
        return 2;
    case ContainsRes::OnEdge:
    case ContainsRes::OnCorner:
        return qFuzzyIsNull(inter_d) ? 4 : 3;
    }
    return 0;
}

void insertOnePoint(const Vec3f& p, Triangulation &tri)
{
    auto tetra_id = tri.contains(p);
    auto new_tetras_id = tri.flip14(tetra_id, p);
    auto p_id = tri.n_vertices() - 1;

    std::stack<Triangulation::TetraIndex> stack;
    stack.push(new_tetras_id[0]);
    stack.push(new_tetras_id[1]);
    stack.push(new_tetras_id[2]);
    stack.push(new_tetras_id[3]);

    while (!stack.empty())
    {
        tetra_id = stack.top();
        stack.pop();

        // Les indices de la stack peuvent ne plus être valides suite aux flips
        if(!tri.is_valid_tetra(tetra_id)) continue;
        auto tetra = tri.tetra(tetra_id);

        // Face opposée à p
        auto opposite_face = tetra.opposite_face(p_id);
        assert(opposite_face != -1);
        auto opposite_face_vertices = tetra.face_vertices(opposite_face);

        // Tétrahèdre voisin (via la face opposée à p)
        auto neighbour_id = tetra.neighbours[opposite_face];

        // Ne pas continuer si le tétrahèdre est au bord
        if(!tri.is_valid_tetra(neighbour_id)) continue;
        auto neighbour = tri.tetra(neighbour_id);

        // Récupération des points
        auto a_id = opposite_face_vertices[0];
        auto a = tri.vertex(a_id).position;
        auto b_id = opposite_face_vertices[1];
        auto b = tri.vertex(b_id).position;
        auto c_id = opposite_face_vertices[2];
        auto c = tri.vertex(c_id).position;
        auto d_id = neighbour.opposite_vertex(neighbour.shared_face(tetra_id));
        auto d = tri.vertex(d_id).position;

        Triangulation::TetraIndex neigh1, neigh2;

        // Permet de savoir si on se trouve dans la configuration 44
        auto isConfig44 = [&]()
        {
            if(neigh1 == 0 || neigh2 == 0 || neigh1 == neigh2) return false;
            if(tri.tetra(neigh1).shared_face(neigh2) == -1) return false;
            return true;
        };

        // Si d se à l'intérieur de la sphère circonscrite à abcp
        if(predicates::inSphere3D(a, b, c, p, d) > 0)
        {
            int index = -1;
            Triangulation::VertexIndex tab[3] = {a_id, b_id, c_id};

            switch(flip_case(a, b, c, d, p, &index))
            {
            case 1:
            case 4: // Flip 2 -> 3
            {
                auto new_t_id = tri.flip23(tetra_id, neighbour_id);
                stack.push(new_t_id[0]);
                stack.push(new_t_id[1]);
                stack.push(new_t_id[2]);
                break;
            }
            case 2: // Flip 3 -> 2
            {
                assert(index != -1);
                auto v_id = tab[index];
                neigh1 = tetra.neighbours[tetra.opposite_face(v_id)];
                neigh2 = neighbour.neighbours[neighbour.opposite_face(v_id)];
                if(neigh1 == neigh2 && neigh1 != 0) // Si ils ont le même voisin
                {
                    auto new_t_id = tri.flip32(tetra_id, neighbour_id, neigh1);
                    stack.push(new_t_id[0]);
                    stack.push(new_t_id[1]);
                }
                break;
            }
            case 3: // Flip 4 -> 4
            {
                assert(index != -1);
                auto v_id = tab[index];
                neigh1 = tetra.neighbours[tetra.opposite_face(v_id)];
                neigh2 = neighbour.neighbours[neighbour.opposite_face(v_id)];
                if(isConfig44())
                {
                    auto new_t_id = tri.flip44(tetra_id, neighbour_id, neigh1, neigh2);
                    stack.push(new_t_id[0]);
                    stack.push(new_t_id[1]);
                    stack.push(new_t_id[2]);
                    stack.push(new_t_id[3]);
                }
                break;
            }
            }
        }
    }
}

void algo::delaunay(const std::vector<Vec3f> &seeds, Triangulation &tri)
{
    for(auto &seed : seeds)
    {
        insertOnePoint(seed, tri);
    }
}

std::pair<Vec3f, float> medianPlan(const Vec3f& P1, const Vec3f& P2)
{
    auto I = P1 + (P2 - P1) * 0.5;
    auto n = (P2 - P1).normalized();
    auto d = n.dot(I);
    return std::make_pair(n, d);
}

Vec3f sphereCenter(const Triangulation &tri, Triangulation::TetraIndex tetra_id)
{
    auto tetra = tri.tetra(tetra_id);

    auto A = tri.vertex(tetra.vertices[0]).position,
         B = tri.vertex(tetra.vertices[1]).position,
         C = tri.vertex(tetra.vertices[2]).position,
         D = tri.vertex(tetra.vertices[3]).position;

    auto med_AB = medianPlan(A, B),
         med_BC = medianPlan(B, C),
         med_CD = medianPlan(C, D);

    Mat3f mat;
    mat << med_AB.first[0], med_AB.first[1], med_AB.first[2],
           med_BC.first[0], med_BC.first[1], med_BC.first[2],
           med_CD.first[0], med_CD.first[1], med_CD.first[2];

    Vec3f vec;
    vec << med_AB.second, med_BC.second, med_CD.second;

    return mat.colPivHouseholderQr().solve(vec);
}

struct Plane {
    Vec3f point, normal;
};

typedef std::vector<Plane> Cell;
typedef std::vector<Cell> Cells;

void dual(const Triangulation &tri, Cells& cells) {
    std::vector<Triangulation::TetraIndex> neighbours;
    std::vector<Vec3f> points;

    cells.clear();
    cells.reserve(tri.n_vertices());
    for (size_t i = 0; i < tri.n_vertices(); ++i) {
        tri.neighbour_tetras(i, neighbours);
        points.clear();
        for (auto tetra : neighbours) {
            points.push_back(sphereCenter(tri, tetra));
        }
        Mesh hull = convexHullCGAL(points);
        hull.update_face_normals();

        Cell cell;
        cell.reserve(hull.n_faces());
        for (auto f : hull.faces()) {
            Plane p;
            p.point = hull.point(hull.to_vertex_handle(hull.halfedge_handle(f)));
            p.normal = hull.normal(f);
            cell.push_back(p);
        }
        cells.push_back(cell);
    }
}

void algo::dual(const Triangulation &tri, Mesh &result)
{
    std::vector<Triangulation::TetraIndex> neighbours;
    std::vector<Vec3f> points;

    result.clean_keep_reservation();

    for (auto it = tri.tetras_begin(); it != tri.tetras_end(); ++it) {
        result.add_vertex(sphereCenter(tri, it.index()));
    }

    for (size_t i = 0; i < tri.n_vertices(); ++i) {
        tri.neighbour_tetras(i, neighbours);
        points.clear();
        for (auto tetra : neighbours) {
            points.push_back(sphereCenter(tri, tetra));
        }
        Mesh hull = convexHullCGAL(points);

        int first_v_idx = result.n_vertices();
        for (auto v : hull.vertices()) {
            result.add_vertex(hull.point(v));
        }

        for (auto f : hull.faces()) {
            auto heh = hull.halfedge_handle(f);
            auto v1 = hull.to_vertex_handle(heh);
            heh = hull.next_halfedge_handle(heh);
            auto v2 = hull.to_vertex_handle(heh);
            heh = hull.next_halfedge_handle(heh);
            auto v3 = hull.to_vertex_handle(heh);
            result.add_face(result.vertex_handle(first_v_idx + v1.idx()),
                            result.vertex_handle(first_v_idx + v2.idx()),
                            result.vertex_handle(first_v_idx + v3.idx()));
        }
    }
}

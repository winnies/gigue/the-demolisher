#include <random>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>
#include <QMimeData>
#include <QSettings>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "addobjectdialog.h"
#include "randompointsparams.h"
#include "meshparams.h"
#include "triangulationparams.h"
#include "algo.h"

static const char* SESSION_FILENAME = "session.bin";
static const quint32 SESSION_VERSION = 2;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_objectModel(new ObjectModel(this)),
    voronoiDiag(new VoronoiDialog(m_objectModel, this))
{
    setAcceptDrops(true);
    ui->setupUi(this);
    ui->objects->setModel(m_objectModel);
    connect(ui->actionCharger_session, &QAction::triggered, this, &MainWindow::loadSession);
    connect(ui->actionEnregistrer_session, &QAction::triggered, this, &MainWindow::saveSession);
    connect(ui->actionQuitter, &QAction::triggered, this, &MainWindow::close);
    connect(ui->addObject, &QToolButton::clicked, this, &MainWindow::addObject);
    connect(ui->removeObject, &QToolButton::clicked, this, &MainWindow::removeCurrentObject);
    connect(ui->objectName, &QLineEdit::editingFinished, this, &MainWindow::renameCurrentObject);
    connect(ui->objects, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &MainWindow::showCurrentObject, Qt::QueuedConnection);
    connect(ui->objectShown, &QToolButton::clicked, this, &MainWindow::showChanged);
    connect(ui->centerObject, &QToolButton::clicked, this, &MainWindow::centerCurrentObject);

    showCurrentObject();

    QSettings settings;
    ui->actionOuverture_auto_de_la_session->setChecked(
                settings.value("mainwindow/actionOuverture_auto_de_la_session",
                               ui->actionOuverture_auto_de_la_session->isChecked()).toBool());
    ui->actionEnregistrement_auto_de_la_session->setChecked(
                settings.value("mainwindow/actionEnregistrement_auto_de_la_session",
                               ui->actionEnregistrement_auto_de_la_session->isChecked()).toBool());

    if (ui->actionOuverture_auto_de_la_session->isChecked() && QFile::exists(SESSION_FILENAME)) {
        show();
        loadSession();
    }
}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (ui->actionEnregistrement_auto_de_la_session->isChecked()) {
        saveSession();
    }

    QSettings settings;
    settings.setValue("mainwindow/actionOuverture_auto_de_la_session", ui->actionOuverture_auto_de_la_session->isChecked());
    settings.setValue("mainwindow/actionEnregistrement_auto_de_la_session", ui->actionEnregistrement_auto_de_la_session->isChecked());

    event->accept();
}

void MainWindow::addObject(){
    auto dialog = new AddObjectDialog(this);
    if (dialog->exec() == QDialog::Rejected) {
        return;
    }

    auto obj = dialog->getObject();

    auto addObject = [&](Object * object, QString name = "") {
        object->name = QString("%1 (%2)").arg(name.isEmpty() ? obj : name).arg(nextObjectId++);
        ui->objectWidget->addWidget(object->params);
        m_objectModel->insert(std::unique_ptr<Object>(object));
    };

    if (obj == "Maillage vide") {
        auto o = new MeshObject;
        o->params = new MeshParams(ui->openGLWidget, o);
        addObject(o);
    } else if (obj == "Maillage depuis un ou plusieurs fichiers") {
        auto fileNames = QFileDialog::getOpenFileNames(this, tr("Ouvrir des maillages"), "", QString::fromStdString(OpenMesh::IO::IOManager().qt_read_filters()));
        if (fileNames.isEmpty()) {
            return;
        }
        for (const auto& fileName : fileNames) {
            auto o = addObjectMaillage(fileName);
            if(o == nullptr) continue;
            addObject(o, QFileInfo(fileName).fileName());
        }
    } else if (obj == "Points aléatoires") {
        auto o = new PointsObject;
        o->params = new RandomPointsParams(ui->openGLWidget, o);
        addObject(o);
    } else if (obj == "Triangulation") {
        auto o = new TriangulationObject;
        o-> params = new TriangulationParams(ui->openGLWidget, o);
        addObject(o);
    }

    ui->objects->setCurrentIndex(m_objectModel->rowCount()-1);
}

MeshObject* MainWindow::addObjectMaillage (const QString& fileName) {
    Mesh mesh;

    if (!mesh.read(fileName.toStdString())) {
        QMessageBox::critical(this, tr("Lecture d'un maillage"), tr("Une erreur est survenue lors de la lecture de %1").arg(fileName));
        return nullptr;
    }

    auto o = new MeshObject;
    o->fileName = fileName;
    o->mesh = std::move(mesh);
    o->params = new MeshParams(ui->openGLWidget, o);

    return o;
}

void MainWindow::removeCurrentObject()
{
    auto index = ui->objects->currentIndex();
    if (index == -1) return;
    m_objectModel->remove(index);
}

void MainWindow::renameCurrentObject()
{
    auto index = ui->objects->currentIndex();
    if (index == -1) return;
    m_objectModel->setName(index, ui->objectName->text());
}

void MainWindow::showChanged(bool checked)
{
    auto index = ui->objects->currentIndex();
    if (index == -1) return;
    m_objectModel->get(index).setIsShown(checked);
    ui->objectShown->setIcon(QIcon::fromTheme(checked ? "view-visible" : "view-hidden"));
}

void MainWindow::showCurrentObject()
{
    auto index = ui->objects->currentIndex();

    ui->objects->setEnabled(index != -1);
    ui->objectShown->setEnabled(index != -1);
    ui->centerObject->setEnabled(index != -1);
    ui->objectName->setEnabled(index != -1);
    ui->removeObject->setEnabled(index != -1);

    if (index == -1) {
        ui->objectName->setText("");
    } else {
        bool isShown = m_objectModel->get(index).isShown();
        ui->objectShown->setChecked(isShown);
        ui->objectShown->setIcon(QIcon::fromTheme(isShown ? "view-visible" : "view-hidden"));
        ui->objectName->setText(m_objectModel->getName(index));
        ui->objectWidget->setCurrentWidget(m_objectModel->get(index).params);
    }
}

void MainWindow::centerCurrentObject()
{
    auto index = ui->objects->currentIndex();
    if (index == -1) return;
    m_objectModel->get(index).params->centerObject();
}

extern myOpenGLWidget* objectReadView;


void MainWindow::loadSession()
{
    ui->statusBar->showMessage("Chargement en cours...");

    while(ui->objectWidget->count() > 0) {
        delete ui->objectWidget->widget(0);
    }

    QFile file(SESSION_FILENAME);
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::warning(this, tr("Chargement de la session"), tr("Aucune session encore sauvegardée."));
        ui->statusBar->clearMessage();
        return;
    }

    QDataStream in(&file);
    objectReadView = ui->openGLWidget;

    quint32 file_session_version;
    in >> file_session_version;
    if (file_session_version != SESSION_VERSION) {
        QMessageBox::critical(this, tr("Chargement de la session"),
                              tr("La dernière session sauvegardée n'est pas compatible (format v%1) avec la version actuelle du logiciel (format v%2).")
                              .arg(file_session_version).arg(SESSION_VERSION));
        ui->statusBar->clearMessage();
        return;
    }

    QRect geo; in >> geo; setGeometry(geo);
    in >> nextObjectId >> *m_objectModel >> *ui->openGLWidget >> *voronoiDiag;
    objectReadView = nullptr;

    for (int i = 0; i < m_objectModel->rowCount(); ++i) {
        ui->objectWidget->addWidget(m_objectModel->get(i).params);
    }
    ui->objectWidget->setCurrentIndex(0);

    ui->statusBar->showMessage("Chargement terminé", 2000);
}

void MainWindow::saveSession()
{
    ui->statusBar->showMessage("Sauvegarde en cours...");

    QFile file(SESSION_FILENAME);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    out << SESSION_VERSION << geometry()
        << nextObjectId << *m_objectModel << *ui->openGLWidget << *voronoiDiag;

    ui->statusBar->showMessage("Sauvegarde terminée", 2000);
}

/* **** Event **** */
void MainWindow::dragEnterEvent(QDragEnterEvent* event){
    if (event->mimeData()->hasUrls()) {
        event->acceptProposedAction();
    }
}

void MainWindow::dropEvent(QDropEvent* event){
    const QMimeData* file = event->mimeData();
    if (file->hasUrls()) {
        if (file->urls().isEmpty()) {
            return;
        }
        for (auto i : file->urls()) {
            auto fileName = i.toLocalFile();
            auto object = addObjectMaillage(fileName);
            if (object == nullptr) continue;
            object->name = QString("%1 (%2)").arg(QFileInfo(fileName).fileName()).arg(nextObjectId++);
            ui->objectWidget->addWidget(object->params);
            m_objectModel->insert(std::unique_ptr<Object>(object));
            event->acceptProposedAction();
        }
    }

    ui->objects->setCurrentIndex(m_objectModel->rowCount()-1);
}


void MainWindow::on_actionVoronoi_triggered()
{
    voronoiDiag->show();
}

void MainWindow::on_actionRecentrer_l_origine_triggered()
{
    ui->openGLWidget->set_scene_pos({0, 0, 0}, 1.f);
}

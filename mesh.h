#ifndef MESH_H
#define MESH_H

#include "typedef.h"

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

// version modifiée de OpenMesh/Core/Geometry/EigenVectorT.hh

namespace OpenMesh {
  template <typename _Scalar, int _Rows, int _Cols, int _Options>
  struct vector_traits<Eigen::Matrix<_Scalar, _Rows, _Cols, _Options>> {
      static_assert(_Rows != Eigen::Dynamic && _Cols != Eigen::Dynamic,
                    "Should not use dynamic vectors.");
      static_assert(_Rows == 1 || _Cols == 1, "Should not use matrices.");

      using vector_type = Eigen::Matrix<_Scalar, _Rows, _Cols, _Options>;
      using value_type = _Scalar;
      static const size_t size_ = _Rows * _Cols;
      static size_t size() { return size_; }
};

} // namespace OpenMesh

namespace Eigen {

  template <typename Derived>
  typename Derived::Scalar dot(const MatrixBase<Derived> &x,
                               const MatrixBase<Derived> &y) {
      return x.dot(y);
  }

  template <typename Derived>
  typename MatrixBase< Derived >::PlainObject cross(const MatrixBase<Derived> &x, const MatrixBase<Derived> &y) {
      return x.cross(y);
  }

  template <typename Derived>
  typename Derived::Scalar norm(const MatrixBase<Derived> &x) {
      return x.norm();
  }

  template <typename Derived>
  typename Derived::Scalar sqrnorm(const MatrixBase<Derived> &x) {
      return x.dot(x);
  }

  template <typename Derived>
  typename MatrixBase<Derived>::PlainObject normalize(MatrixBase<Derived> &x) {
      x /= x.norm();
      return x;
  }

  template <typename Derived>
  MatrixBase<Derived> &vectorize(MatrixBase<Derived> &x,
                                 typename Derived::Scalar const &val) {
      x.fill(val);
      return x;
  }

} // namespace Eigen

struct MyTraits : public OpenMesh::DefaultTraits
{
    using Point = Vec3f;
    using Normal = Vec3f;
    typedef OpenMesh::Vec3f Color;
    VertexAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color );
    HalfedgeAttributes( OpenMesh::Attributes::PrevHalfedge );
    FaceAttributes ( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color );
    EdgeAttributes( OpenMesh::Attributes::Color );
};

class Mesh : public OpenMesh::TriMesh_ArrayKernelT<MyTraits> {
public:
    using TriMesh_ArrayKernelT<MyTraits>::TriMesh_ArrayKernelT;

    bool read(const std::string& filename);

    bool write(const std::string& filename);

    // calcule le barycentre de tous les sommets d'un mesh
    Point calc_barycenter() const;


    // structure pour la boîte englobante
    struct SafeBox {
        Point min;
        Point max;
    };

    // calcule la boite englobante d'un mesh
    SafeBox calc_safe_box() const;

    void reset_colors();
};

namespace OpenMesh {
template <>
struct color_caster<Vec4ui, Vec3f>
{
  typedef Vec4ui return_type;

  inline static return_type cast(const Vec3f& _src)
  {
    // basé sur OpenMesh/Core/Utils/color_cast.hh
    return Vec4ui( (unsigned int)(_src[0]* 255.0f + 0.5f),
                   (unsigned int)(_src[1]* 255.0f + 0.5f),
                   (unsigned int)(_src[2]* 255.0f + 0.5f),
                   255 );

  }
};
}

#endif // MESH_H

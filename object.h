#ifndef OBJECT_H
#define OBJECT_H

#include <memory>
#include <QString>
#include <QPointer>
class Params;

/*!
 * \brief Représente un objet affichable dans l'application.
 *
 * Note : il faut penser à appeler \a dataChanged quand les données de l'objet sont changées.
 *
 */
class Object : public QObject
{
    Q_OBJECT

public:
    enum class Type {
        Points,
        Mesh,
        Triangulation
    };

    Object(Type type, QObject * parent = nullptr);
    virtual ~Object();

    const Type type;

    /*!
     * \brief le nom de l'objet.
     */
    QString name;

    /*!
     * \brief le widget qui permet de configurer l'objet.
     */
    QPointer<Params> params;

    /*!
     * \brief Indique si l'objet est affiché.
     * \return vrai si l'objet est affiché
     */
    bool isShown() const;

    /*!
     * \brief Change l'état de l'affichage de l'objet.
     * \param isShown vrai si l'objet doit être affiché
     */
    void setIsShown(bool isShown);

signals:
    void dataChanged();
    void isShownChanged(bool isShown);

protected:
    virtual void write(QDataStream&) {};
    virtual void read(QDataStream&) {};

private:
    bool m_isShown;

    friend QDataStream &operator<<(QDataStream &, const std::unique_ptr<Object> &);
    friend QDataStream &operator>>(QDataStream &, std::unique_ptr<Object> &);
};

using ObjectPtr = std::unique_ptr<Object>;

#include "typedef.h"

class PointsObject : public Object {
    Q_OBJECT

public:
    PointsObject(Object * parent = nullptr);

    std::vector<Vec3f> points;

protected:
    void write(QDataStream&);
    void read(QDataStream&);
};

#include "mesh.h"

class MeshObject : public Object {
    Q_OBJECT

public:
    MeshObject(Object * parent = nullptr);

    QString fileName;
    Mesh mesh;

protected:
    void write(QDataStream&);
    void read(QDataStream&);
};

#include "triangulation.h"

class TriangulationObject : public Object {
    Q_OBJECT

public:
    TriangulationObject(Object * parent = nullptr);

    Triangulation tri;

protected:
    void write(QDataStream&);
    void read(QDataStream&);
};

#endif // OBJECT_H

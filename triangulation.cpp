#include "triangulation.h"
#include "predicates.h"
#include "QDebug"

const Triangulation::Tetrahedron::InternalVertexIndex Triangulation::Tetrahedron::faces[4][3] = {
    {0, 2, 1},
    {0, 1, 3},
    {1, 2, 3},
    {0, 3, 2}
};

const Triangulation::Tetrahedron::InternalVertexIndex Triangulation::Tetrahedron::vertex_opposite_to_face[4] = {3, 2, 0, 1};

const Triangulation::Tetrahedron::InternalVertexIndex Triangulation::Tetrahedron::edges[6][2] = {
    {0, 1},
    {1, 2},
    {2, 0},
    {0, 3},
    {1, 3},
    {2, 3}
};

Triangulation::Triangulation()
    : n_valid_tetras(0)
{
}

Triangulation::Triangulation(const std::array<Vec3f, 4> &vs)
    : vertices(vs.begin(), vs.end())
    , n_valid_tetras(0)
    , next_tetra_index(1)
{   
    // first tetra is invalid
    tetras.emplace_back();
    next_tetra_indexes.emplace_back();

    auto t = new_tetra();
    vertices[0].tetra = t;
    vertices[1].tetra = t;
    vertices[2].tetra = t;
    vertices[3].tetra = t;

    Tetrahedron &c = tetras.back();
    c.vertices = {0, 1, 2, 3};
    c.neighbours = {0, 0, 0, 0};
}

std::array<Triangulation::TetraIndex, 4> Triangulation::flip14(TetraIndex t, const Vec3f &p)
{
    assert(is_valid_tetra(t));
    qDebug() << "filp14" << t << p[0] << p [1] << p[2];

    VertexIndex e = vertices.size();
    vertices.push_back(p);

    VertexIndex v1 = tetras[t].vertices[0],
                v2 = tetras[t].vertices[1],
                v3 = tetras[t].vertices[2],
                v4 = tetras[t].vertices[3];

    TetraIndex n1 = tetras[t].neighbours[0],
               n2 = tetras[t].neighbours[1],
               n3 = tetras[t].neighbours[2],
               n4 = tetras[t].neighbours[3];

    TetraIndex nt1 = t,
               nt2 = new_tetra(),
               nt3 = new_tetra(),
               nt4 = new_tetra();

    tetras[nt1].vertices = {v1, v2, v3, e};
    tetras[nt1].neighbours = {n1, nt2, nt3, nt4};

    tetras[nt2].vertices = {v1, v4, v2, e};
    tetras[nt2].neighbours = {n2, nt4, nt3, nt1};

    tetras[nt3].vertices = {v2, v4, v3, e};
    tetras[nt3].neighbours = {n3, nt2, nt4, nt1};

    tetras[nt4].vertices = {v3, v4, v1, e};
    tetras[nt4].neighbours = {n4, nt3, nt2, nt1};

    update_neighbour(n1, t, nt1);
    update_neighbour(n2, t, nt2);
    update_neighbour(n3, t, nt3);
    update_neighbour(n4, t, nt4);

    vertices[v1].tetra = nt1;
    vertices[v2].tetra = nt1;
    vertices[v3].tetra = nt1;
    vertices[v4].tetra = nt2;
    vertices[e].tetra = nt1;

    return {nt1, nt2, nt3, nt4};
}

std::array<Triangulation::TetraIndex, 3> Triangulation::flip23(TetraIndex t1, TetraIndex t2)
{
    assert(is_valid_tetra(t1) && is_valid_tetra(t2));
    qDebug() << "flip23" << t1 << t2;

    auto shared_face_t1 = tetras[t1].shared_face(t2);
    auto shared_face_t2 = tetras[t2].shared_face(t1);
    assert(shared_face_t1 != -1 && shared_face_t2 != -1);
    auto face_vertices = tetras[t1].face_vertices(shared_face_t1);

    VertexIndex v1 = tetras[t1].opposite_vertex(shared_face_t1),
                v2 = face_vertices[0],
                v3 = face_vertices[1],
                v4 = face_vertices[2],
                v5 = tetras[t2].opposite_vertex(shared_face_t2);

    assert(tetras[t1].find_face_with(v3, v4, v2) != -1);
    assert(tetras[t1].find_face_with(v1, v2, v4) != -1);
    assert(tetras[t1].find_face_with(v1, v4, v3) != -1);
    assert(tetras[t1].find_face_with(v1, v3, v2) != -1);

    assert(tetras[t2].find_face_with(v3, v2, v4) != -1);
    assert(tetras[t2].find_face_with(v5, v4, v2) != -1);
    assert(tetras[t2].find_face_with(v5, v3, v4) != -1);
    assert(tetras[t2].find_face_with(v5, v2, v3) != -1);

    TetraIndex n1 = tetras[t1].neighbours[tetras[t1].find_face_with(v1, v2, v4)],
               n2 = tetras[t1].neighbours[tetras[t1].find_face_with(v1, v4, v3)],
               n3 = tetras[t1].neighbours[tetras[t1].find_face_with(v1, v3, v2)],
               n4 = tetras[t2].neighbours[tetras[t2].find_face_with(v5, v4, v2)],
               n5 = tetras[t2].neighbours[tetras[t2].find_face_with(v5, v3, v4)],
               n6 = tetras[t2].neighbours[tetras[t2].find_face_with(v5, v2, v3)];

    TetraIndex nt1 = t1,
               nt2 = t2,
               nt3 = new_tetra();

    tetras[nt1].vertices = {v2, v4, v5, v1};
    tetras[nt1].neighbours = {n4, n1, nt3, nt2};

    tetras[nt2].vertices = {v3, v2, v5, v1};
    tetras[nt2].neighbours = {n6, n3, nt1, nt3};

    tetras[nt3].vertices = {v4, v3, v5, v1};
    tetras[nt3].neighbours = {n5, n2, nt2, nt1};

    update_neighbour(n1, t1, nt1);
    update_neighbour(n2, t1, nt3);
    update_neighbour(n3, t1, nt2);
    update_neighbour(n4, t2, nt1);
    update_neighbour(n5, t2, nt3);
    update_neighbour(n6, t2, nt2);

    vertices[v1].tetra = nt1;
    vertices[v2].tetra = nt1;
    vertices[v3].tetra = nt2;
    vertices[v4].tetra = nt1;
    vertices[v5].tetra = nt1;

    return {nt1, nt2, nt3};
}

std::array<Triangulation::TetraIndex, 2> Triangulation::flip32(TetraIndex t1, TetraIndex t2, TetraIndex t3)
{
    assert(is_valid_tetra(t1) && is_valid_tetra(t2) && is_valid_tetra(t3));
    qDebug() << "flip32" << t1 << t2 << t3;

    auto shared_face_t2_t1 = tetras[t2].shared_face(t1);
    auto shared_face_t3_t1 = tetras[t3].shared_face(t1);
    assert(shared_face_t2_t1 != -1 && shared_face_t3_t1 != -1);

    VertexIndex v3 = tetras[t2].opposite_vertex(shared_face_t2_t1);
    assert(v3 == tetras[t3].opposite_vertex(shared_face_t3_t1));

    auto v2 = tetras[t1].opposite_vertex(tetras[t1].shared_face(t3));
    auto v4 = tetras[t1].opposite_vertex(tetras[t1].shared_face(t2));

    auto third_v_r = tetras[t1].find_third_vertex(v2, v4);
    assert(third_v_r);
    auto [third_v, third_v_f] = *third_v_r;
    VertexIndex v1 = third_v;
    auto v5 = tetras[t1].opposite_vertex(third_v_f);

    assert(tetras[t1].find_face_with(v1, v2, v4) != -1);
    assert(tetras[t3].find_face_with(v1, v4, v3) != -1);
    assert(tetras[t2].find_face_with(v1, v3, v2) != -1);
    assert(tetras[t1].find_face_with(v5, v4, v2) != -1);
    assert(tetras[t3].find_face_with(v5, v3, v4) != -1);
    assert(tetras[t2].find_face_with(v5, v2, v3) != -1);

    TetraIndex n1 = tetras[t1].neighbours[tetras[t1].find_face_with(v1, v2, v4)],
               n2 = tetras[t3].neighbours[tetras[t3].find_face_with(v1, v4, v3)],
               n3 = tetras[t2].neighbours[tetras[t2].find_face_with(v1, v3, v2)],
               n4 = tetras[t1].neighbours[tetras[t1].find_face_with(v5, v4, v2)],
               n5 = tetras[t3].neighbours[tetras[t3].find_face_with(v5, v3, v4)],
               n6 = tetras[t2].neighbours[tetras[t2].find_face_with(v5, v2, v3)];

    remove_tetra(t3);
    TetraIndex nt1 = t1,
               nt2 = t2;

    tetras[nt1].vertices = {v2, v4, v3, v1};
    tetras[nt1].neighbours = {nt2, n1, n2, n3};

    tetras[nt2].vertices = {v3, v4, v2, v5};
    tetras[nt2].neighbours = {nt1, n5, n4, n6};

    update_neighbour(n1, t1, nt1);
    update_neighbour(n2, t3, nt1);
    update_neighbour(n3, t2, nt1);
    update_neighbour(n4, t1, nt2);
    update_neighbour(n5, t3, nt2);
    update_neighbour(n6, t2, nt2);

    vertices[v1].tetra = nt1;
    vertices[v2].tetra = nt1;
    vertices[v3].tetra = nt1;
    vertices[v4].tetra = nt1;
    vertices[v5].tetra = nt2;

    return {nt1, nt2};
}

std::array<Triangulation::TetraIndex, 4> Triangulation::flip44(TetraIndex t1, TetraIndex t2, TetraIndex t3, TetraIndex t4)
{
    assert(is_valid_tetra(t1) && is_valid_tetra(t2) && is_valid_tetra(t3) && is_valid_tetra(t4));
    qDebug() << "flip44" << t1 << t2 << t3 << t4;

    std::vector<TetraIndex> t1_neighbours;
    t1_neighbours.reserve(3);
    TetraIndex t1_opposite = 0;
    for (auto t : {t2,t3,t4}) {
        if (tetras[t1].shared_face(t) != -1) {
            t1_neighbours.push_back(t);
        } else {
            t1_opposite = t;
        }
    }
    assert(t1_neighbours.size() == 2);
    assert(t1_opposite != 0);
    assert(tetras[t1_opposite].shared_face(t1_neighbours[0]) != -1
        && tetras[t1_opposite].shared_face(t1_neighbours[1]) != -1);

    auto new_ts = flip23(t1, t1_neighbours[0]);
    TetraIndex new_t_flat = 0, new_t_not_flat1, new_t_not_flat2;
    for (short i = 0; i < 3; ++i) {
        auto new_t = new_ts[i];
        if (tetras[new_t].shared_face(t1_neighbours[1]) != -1 && tetras[new_t].shared_face(t1_opposite) != -1) {
            assert(new_t_flat == 0);
            new_t_flat = new_t;
            new_t_not_flat1 = new_ts[(i+1)%3];
            new_t_not_flat2 = new_ts[(i+2)%3];
#ifdef NDEBUG
            break;
#endif
        }
    }
    assert(new_t_flat != 0);

    auto r_flip32 = flip32(t1_neighbours[1], t1_opposite, new_t_flat);

    return {new_t_not_flat1, new_t_not_flat2, r_flip32[0], r_flip32[1]};
}

Triangulation::TetraIndex Triangulation::contains(const Vec3f &point)
{
    auto t_index = tetras_begin().index();
    auto found = false;
    size_t index;
    do { // Si la triangulation est Delaunay, on a forcément arrêt
        for(index = 0; index < 4; index ++)
        {
            auto vertices = tetra(t_index).face_vertices(index);
            auto v1 = vertex(vertices[0]).position;
            auto v2 = vertex(vertices[1]).position;
            auto v3 = vertex(vertices[2]).position;
            float r = predicates::orient3DFast(vertex(vertices[0]).position, vertex(vertices[1]).position, vertex(vertices[2]).position, point);
            if(r >= 0) continue;

            auto next_t = tetra(t_index).neighbours[index];
            t_index = next_t;
            break;
        }
        found = (index == 4);
    } while(!found);
    return t_index;
}

std::vector<Triangulation::TetraIndex> Triangulation::neighbour_tetras(Triangulation::VertexIndex v) const
{
    std::vector<Triangulation::TetraIndex> selected_t;
    neighbour_tetras(v, selected_t);
    return selected_t;
}

void Triangulation::neighbour_tetras(Triangulation::VertexIndex v, std::vector<Triangulation::TetraIndex> &selected_t) const
{
    selected_t.clear();
    // Initialisation (ajout d'un premier élément)
    selected_t.push_back(vertex(v).tetra);

    for(size_t current_t_index = 0; current_t_index < selected_t.size(); ++current_t_index)
    {
        auto current_t = tetra(selected_t[current_t_index]);
        auto opposite_face = current_t.opposite_face(v); // Face opposée

        for(short f = 0; f < 4; ++f)
        {
            auto neigh = current_t.neighbours[f];
            // On ajoute les tétrahèdres voisins qui partagent le sommet (si on ne l'a pas déjà fait)
            if(neigh != 0 && f != opposite_face && std::find(selected_t.begin(), selected_t.end(), neigh) == selected_t.end())
            {
                selected_t.push_back(neigh);
            }
        }
    }
}

void Triangulation::update_neighbour(TetraIndex neighbour, TetraIndex old_tetra, TetraIndex new_tetra)
{
    if (neighbour == 0) return;
    for (TetraIndex& n : tetras[neighbour].neighbours) {
        if (n == old_tetra) {
            n = new_tetra;
            return;
        }
    }
    assert(false);
}

Triangulation::TetraIndex Triangulation::new_tetra()
{
    n_valid_tetras += 1;

    auto index = next_tetra_index;
    if (index == tetras.size()) {
        tetras.emplace_back();
        next_tetra_indexes.emplace_back(0);
        next_tetra_index += 1;
    } else {
        next_tetra_index = next_tetra_indexes[index];
    }
    next_tetra_indexes[index] = 0;
    return index;
}

void Triangulation::remove_tetra(TetraIndex t)
{
    n_valid_tetras -= 1;

    next_tetra_indexes[t] = next_tetra_index;
    next_tetra_index = t;
}

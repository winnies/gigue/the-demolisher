#include "predicates.h"

extern "C" void exactinit();
extern "C" float orient3d(float*, float*, float*, float*);
extern "C" float orient3dfast(float*, float*, float*, float*);
extern "C" float insphere(float*, float*, float*, float*, float*);

void predicates::init()
{
    exactinit();
}

float predicates::orient3D(const Vec3f &a, const Vec3f &b, const Vec3f &c, const Vec3f &d)
{
    return orient3d(const_cast<float*>(a.data()), const_cast<float*>(b.data()), const_cast<float*>(c.data()), const_cast<float*>(d.data()));
}

float predicates::orient3DFast(const Vec3f &a, const Vec3f &b, const Vec3f &c, const Vec3f &d)
{
    return orient3dfast(const_cast<float*>(a.data()), const_cast<float*>(b.data()), const_cast<float*>(c.data()), const_cast<float*>(d.data()));
}

float predicates::inSphere3D(const Vec3f &a, const Vec3f &b, const Vec3f &c, const Vec3f &d, const Vec3f &e)
{
    return insphere(const_cast<float*>(a.data()), const_cast<float*>(b.data()), const_cast<float*>(c.data()), const_cast<float*>(d.data()), const_cast<float*>(e.data()));
}

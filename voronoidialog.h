#ifndef VORONOIDIALOG_H
#define VORONOIDIALOG_H

#include <QDialog>
#include "objectmodel.h"

namespace Ui {
class VoronoiDialog;
}

class VoronoiDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VoronoiDialog(ObjectModel *objectModel, QWidget *parent = nullptr);
    ~VoronoiDialog();

private slots:
    void on_runSteps_clicked();

private:
    Ui::VoronoiDialog *ui;
    ObjectModel *objectModel;
    ObjectTypeFilterModel *meshFilter, *pointsFilter, *triFilter;

    friend QDataStream &operator<<(QDataStream &, const VoronoiDialog &);
    friend QDataStream &operator>>(QDataStream &, VoronoiDialog &);
};

#endif // VORONOIDIALOG_H

#include "mesh.h"
#include <iostream>

bool Mesh::read(const std::string &filename)
{
    std::clog << "Lecture de " << filename << std::endl;

    using OpenMesh::IO::Options;
    Options opt;
    opt += Options::FaceNormal;
    opt += Options::VertexNormal;
    opt += Options::VertexColor;
    opt += Options::EdgeColor;
    opt += Options::FaceColor;
    if (!OpenMesh::IO::read_mesh(*this, filename, opt)) {
      return false;
    }

    if (!opt.face_has_normal()) {
      std::clog << "Calcul des normales aux faces." << std::endl;
      update_face_normals();
    }

    if (!opt.vertex_has_normal()) {
      std::clog << "Calcul des normales aux sommets." << std::endl;
      update_vertex_normals();
    }

    if (!opt.vertex_has_color()) {
        std::clog << "Utilisation de couleurs standards pour les sommets." << std::endl;

        for (auto vertex : vertices()) {
            set_color(vertex, {0, 0, 0});
        }
    }

    if (!opt.edge_has_color()) {
        std::clog << "Utilisation de couleurs standards pour les arêtes." << std::endl;

        for (auto edge : edges()) {
            set_color(edge, {0, 0, 0});
        }
    }

    if (!opt.face_has_color()) {
        std::clog << "Utilisation de couleurs standards pour les faces." << std::endl;

        for (auto face : faces()) {
            set_color(face, {0.58, 0.58, 0.58});
        }
    }

    return true;
}

bool Mesh::write(const std::string &filename)
{
    return OpenMesh::IO::write_mesh(*this, filename);
}

Mesh::Point Mesh::calc_barycenter() const {
    Point cog;
    cog[0] = cog[1] = cog[2] = 0.0;
    for(auto v_it = vertices_begin(); v_it != vertices_end(); v_it++) {
        cog += point(*v_it);
    }
    cog /= n_vertices();
    return cog;
}

Mesh::SafeBox Mesh::calc_safe_box() const {
    SafeBox sb;
    sb.max = Point::Constant(std::numeric_limits<float>::lowest());
    sb.min = Point::Constant(std::numeric_limits<float>::max());

    for(auto v_it = vertices_begin(); v_it != vertices_end(); v_it++) {
        const auto& currentPoint = point(*v_it);
        sb.max = sb.max.cwiseMax(currentPoint);
        sb.min = sb.min.cwiseMin(currentPoint);
    }

    return sb;
}

void Mesh::reset_colors()
{
    for (auto vertex : vertices()) {
        set_color(vertex, {0, 0, 0});
    }

    for (auto edge : edges()) {
        set_color(edge, {0, 0, 0});
    }

    for (auto face : faces()) {
        set_color(face, {0.58, 0.58, 0.58});
    }
}

attribute highp vec4 posAttr;
attribute lowp vec4 colAttr;
attribute lowp vec4 colFaceAttr;
attribute highp vec3 norAttr;
attribute highp vec3 norFaceAttr;
varying lowp vec4 col;
varying highp vec3 nor;
uniform highp mat4 matrix;
uniform highp mat3 norMat;
uniform bool useFaceCol;
uniform bool useFaceNor;

void main() {
   col = useFaceCol ? colFaceAttr : colAttr;
   gl_Position = matrix * posAttr;
   nor = norMat * (useFaceNor ? norFaceAttr : norAttr);
}

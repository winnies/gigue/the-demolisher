#include "triangulationparams.h"
#include "ui_triangulationparams.h"
#include <unordered_set>
#include <QMessageBox>

TriangulationParams::TriangulationParams(myOpenGLWidget *view, TriangulationObject *tri, QWidget *parent) :
    Params(Type::Triangulation, parent),
    ui(new Ui::TriangulationParams),
    view(view),
    tri(tri)
{
    ui->setupUi(this);
    connect(tri, &TriangulationObject::dataChanged, this, &TriangulationParams::loadTri);
    connect(tri, &TriangulationObject::isShownChanged, this, &TriangulationParams::updateView);
    connect(ui->create, &QPushButton::clicked, this, &TriangulationParams::createTri);
    connect(ui->showPoint, &QCheckBox::toggled, this, &TriangulationParams::updateView);
    connect(ui->showTetra, &QGroupBox::toggled, this, &TriangulationParams::showTri);
    connect(ui->showTetraIndex, QOverload<int>::of(&QSpinBox::valueChanged), this, &TriangulationParams::showTri);
    connect(ui->showTetraNeighbours, &QCheckBox::stateChanged, this, &TriangulationParams::showTri);
    connect(ui->showTetraShowFaces, &QCheckBox::stateChanged, this, &TriangulationParams::updateView);
    connect(ui->showTetraAutoCenter, &QCheckBox::stateChanged, this, &TriangulationParams::showTri);
    connect(ui->showTetraOffset, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &TriangulationParams::showTri);
    connect(ui->flip14, &QPushButton::clicked, this, &TriangulationParams::flip14);
    connect(ui->flip23, &QPushButton::clicked, this, &TriangulationParams::flip23);
    connect(ui->flip32, &QPushButton::clicked, this, &TriangulationParams::flip32);
    connect(ui->flip44, &QPushButton::clicked, this, &TriangulationParams::flip44);
    connect(ui->walk, &QPushButton::clicked, this, &TriangulationParams::walk);

    showTri();
    pointDI = view->drawPoints({{0, 0, 0}}, {qRgb(1, 0, 0)});
    updateView();
}

TriangulationParams::~TriangulationParams()
{
    delete ui;

    if (triDI > 0) {
        view->remove(triDI);
        view->update();
    }

    if (pointDI > 0) {
        view->remove(pointDI);
        view->update();
    }
}

void TriangulationParams::centerObject()
{
    view->set_scene_pos({triCOG[0], triCOG[1], triCOG[2]}, triRadius);
    view->update();
}

void TriangulationParams::write(QDataStream &s) const
{
    {
        for (auto * spinbox : {ui->vertexX_1, ui->vertexY_1, ui->vertexZ_1,
                               ui->vertexX_2, ui->vertexY_2, ui->vertexZ_2,
                               ui->vertexX_3, ui->vertexY_3, ui->vertexZ_3,
                               ui->vertexX_4, ui->vertexY_4, ui->vertexZ_4,
                               ui->showTetraOffset,
                               ui->newVertexX, ui->newVertexY, ui->newVertexZ,
                               ui->walkX, ui->walkY, ui->walkZ}) {
            s << spinbox->value();
        }
    }
    {
        s << ui->showTetra->isChecked();
        for (auto * checkbox : {ui->showPoint, ui->showTetraNeighbours, ui->showTetraShowFaces, ui->showTetraAutoCenter}) {
            s << checkbox->isChecked();
        }
    }
    {
        s << ui->tabWidget->currentIndex();
        for (auto * spinbox : {ui->showTetraIndex, ui->flipTetra1, ui->flipTetra2, ui->flipTetra3, ui->flipTetra4}) {
            s << spinbox->value();
        }
    }
}

void TriangulationParams::read(QDataStream &s)
{
    {
        double tmp;
        for (auto * spinbox : {ui->vertexX_1, ui->vertexY_1, ui->vertexZ_1,
                               ui->vertexX_2, ui->vertexY_2, ui->vertexZ_2,
                               ui->vertexX_3, ui->vertexY_3, ui->vertexZ_3,
                               ui->vertexX_4, ui->vertexY_4, ui->vertexZ_4,
                               ui->showTetraOffset,
                               ui->newVertexX, ui->newVertexY, ui->newVertexZ,
                               ui->walkX, ui->walkY, ui->walkZ}) {
            s >> tmp; spinbox->setValue(tmp);
        }
    }
    {
        bool tmp;
        s >> tmp; ui->showTetra->setChecked(tmp);
        for (auto * checkbox : {ui->showPoint, ui->showTetraNeighbours, ui->showTetraShowFaces, ui->showTetraAutoCenter}) {
            s >> tmp; checkbox->setChecked(tmp);
        }
    }
    {
        int tmp;
        s >> tmp; ui->tabWidget->setCurrentIndex(tmp);
        for (auto * spinbox : {ui->showTetraIndex, ui->flipTetra1, ui->flipTetra2, ui->flipTetra3, ui->flipTetra4}) {
            s >> tmp; spinbox->setValue(tmp);
        }
    }
}

void TriangulationParams::loadTri()
{
    if (tri->tri.n_tetras() > 0) {
        triCOG = calcCOG();
        triRadius = calcRadius(triCOG);
    } else {
        triCOG = {0.f, 0.f, 0.f};
        triRadius = 1.f;
    }
    showTri();
    updateView(); // TODO showTri tri null quand showTri constructeur
}

struct TetraEdge {
    Triangulation::VertexIndex v1, v2;
    TetraEdge (Triangulation::VertexIndex _v1, Triangulation::VertexIndex _v2)
        :v1(_v1), v2(_v2){
        if (v1 > v2) {
            std::swap(v1, v2);
        }
    }

    bool operator==(const TetraEdge& t) const {
        return v1 == t.v1 && v2 == t.v2;
    }
};

// custom specialization of std::hash can be injected in namespace std
namespace std
{
    template<> struct hash<TetraEdge>
    {
        std::size_t operator()(TetraEdge const& e) const noexcept
        {
            return e.v1 ^ (e.v2 << 1); // or use boost::hash_combine (see Discussion)
        }
    };
}

void TriangulationParams::showTri()
{
    static Mesh mesh;
    mesh.clean_keep_reservation();

    const auto& tri = this->tri->tri;

    Triangulation::TetraIndex tetraToShow = 0;
    std::array<Triangulation::TetraIndex, 4> tetraToShowNeigbours;
    std::unordered_set<TetraEdge> tetraToShowEdges, tetraToShowNeigboursEdges;
    bool useOffset = false;
    Mesh::Point tetraToShowCOG;

    auto t_idx = (Triangulation::TetraIndex)ui->showTetraIndex->value();
    if (ui->showTetra->isChecked() && tri.is_valid_tetra(t_idx)) {
        const auto& tetra = tri.tetra(t_idx);
        tetraToShow = t_idx;
        tetraToShowNeigbours = tetra.neighbours;

        for (Triangulation::Tetrahedron::EdgeIndex i = 0; i < 6; ++i) {
            auto e = tetra.edge(i);
            tetraToShowEdges.emplace(e[0], e[1]);
        }

        for (auto n : tetra.neighbours) {
            if (n == 0) continue;
            for (Triangulation::Tetrahedron::EdgeIndex i = 0; i < 6; ++i) {
                auto e = tri.tetra(n).edge(i);
                tetraToShowNeigboursEdges.emplace(e[0], e[1]);
            }
        }

        useOffset = ui->showTetraOffset->value() > 0;
        tetraToShowCOG = calcTriCOG(tetraToShow);

        if (ui->showTetraAutoCenter->isChecked()) {
            view->set_scene_pos({tetraToShowCOG[0], tetraToShowCOG[1], tetraToShowCOG[2]},
                                 calcTriRadius(tetraToShow, tetraToShowCOG));
        }
    }

    mesh.reserve(tri.n_tetras() * 4, tri.n_tetras() * 6, tri.n_tetras() * 4);

    for (auto tetra_it = tri.tetras_begin(); tetra_it.is_valid(); ++tetra_it) {
        const auto& vertices = tetra_it->vertices;
        std::array<Mesh::VertexHandle, 4> vhs {mesh.add_vertex(tri.vertex(vertices[0]).position),
                                               mesh.add_vertex(tri.vertex(vertices[1]).position),
                                               mesh.add_vertex(tri.vertex(vertices[2]).position),
                                               mesh.add_vertex(tri.vertex(vertices[3]).position)};

        if (useOffset && tetra_it.index() != tetraToShow) {
            auto offset = (calcTriCOG(tetra_it.index()) - tetraToShowCOG) * ui->showTetraOffset->value();
            for (auto vh : vhs) {
                mesh.point(vh) += offset;
            }
        }

        for (Triangulation::Tetrahedron::FaceIndex f = 0; f < 4; ++f) {
            auto indices = Triangulation::Tetrahedron::faces[f];

            auto fh = mesh.add_face(vhs[indices[0]], vhs[indices[1]], vhs[indices[2]]);

            for (short i = 0; i < 3; ++i) {
                auto v1 = indices[i], v2 = indices[(i+1)%3];
                auto eh = mesh.edge_handle(mesh.find_halfedge(vhs[v1], vhs[v2]));
                TetraEdge te(vertices[v1], vertices[v2]);
                Mesh::Color c(0, 0, 0);
                if (tetraToShowEdges.find(te) != tetraToShowEdges.end()) {
                    c = {1, 0, 0};
                } else if (ui->showTetraNeighbours->isChecked()
                        && tetraToShowNeigboursEdges.find(te) != tetraToShowNeigboursEdges.end()) {
                    c = {0, 0, 1};
                }
                mesh.set_color(eh, c);
            }

            Mesh::Color c(0.58, 0.58, 0.58);
            if (tetraToShow == tetra_it.index()) {
                c = {1, 0, 0};
            } else if (ui->showTetraNeighbours->isChecked()
                    && std::find(tetraToShowNeigbours.begin(), tetraToShowNeigbours.end(), tetra_it.index()) != tetraToShowNeigbours.end()) {
                c = {0, 0, 1};
            }
            mesh.set_color(fh, c);
        }
    }

    mesh.update_face_normals();
    triDI = view->drawMesh(mesh, triDI);
    view->update();
}

void TriangulationParams::updateView()
{
    view->isShown(pointDI) = ui->showPoint->isChecked();

    if (triDI == 0) return;

    DrawMode drawMode = DrawMode::Line;
    if (ui->showTetraShowFaces->isChecked()) {
        drawMode = drawMode | DrawMode::Triangle;
    }

    view->isShown(triDI) = tri->isShown();
    view->drawMode(triDI) = drawMode;
    view->useFaceColor(triDI) = true;
    view->flatLigthing(triDI) = true;
    view->ligthing(triDI) = ui->showTetraShowFaces->isChecked();    
    view->update();
}

void TriangulationParams::createTri()
{
    std::array<Vec3f, 4> vertices{
        Vec3f(ui->vertexX_1->value(), ui->vertexY_1->value(), ui->vertexZ_1->value()),
        Vec3f(ui->vertexX_2->value(), ui->vertexY_2->value(), ui->vertexZ_2->value()),
        Vec3f(ui->vertexX_3->value(), ui->vertexY_3->value(), ui->vertexZ_3->value()),
        Vec3f(ui->vertexX_4->value(), ui->vertexY_4->value(), ui->vertexZ_4->value())};
    tri->tri = Triangulation(vertices);
    emit tri->dataChanged();
}

void TriangulationParams::flip14()
{
    auto& tri = this->tri->tri;
    if (tri.n_tetras() == 0) return;

    auto t_idx = (Triangulation::TetraIndex)ui->flipTetra1->value();
    if (!tri.is_valid_tetra(t_idx)) {
        QMessageBox::warning(this, "Paramètres de l'opération flip invalides", "Un ou plusieurs indices ne sont pas valides.");
        return;
    }
    tri.flip14(t_idx,
               Vec3f(ui->newVertexX->value(), ui->newVertexY->value(), ui->newVertexZ->value()));
    emit this->tri->dataChanged();
}

void TriangulationParams::flip23()
{
    auto& tri = this->tri->tri;
    if (tri.n_tetras() == 0) return;

    auto t_idx1 = (Triangulation::TetraIndex)ui->flipTetra1->value();
    auto t_idx2 = (Triangulation::TetraIndex)ui->flipTetra2->value();
    if (!(tri.is_valid_tetra(t_idx1) && tri.is_valid_tetra(t_idx2))) {
        QMessageBox::warning(this, "Paramètres de l'opération flip invalides", "Un ou plusieurs indices ne sont pas valides.");
        return;
    }
    tri.flip23(t_idx1, t_idx2);
    emit this->tri->dataChanged();
}

void TriangulationParams::flip32()
{
    auto& tri = this->tri->tri;
    if (tri.n_tetras() == 0) return;

    auto t_idx1 = (Triangulation::TetraIndex)ui->flipTetra1->value();
    auto t_idx2 = (Triangulation::TetraIndex)ui->flipTetra2->value();
    auto t_idx3 = (Triangulation::TetraIndex)ui->flipTetra3->value();
    if (!(tri.is_valid_tetra(t_idx1) && tri.is_valid_tetra(t_idx2) && tri.is_valid_tetra(t_idx3))) {
        QMessageBox::warning(this, "Paramètres de l'opération flip invalides", "Un ou plusieurs indices ne sont pas valides.");
        return;
    }
    tri.flip32(t_idx1, t_idx2, t_idx3);
    emit this->tri->dataChanged();
}

void TriangulationParams::flip44()
{
    auto& tri = this->tri->tri;
    if (tri.n_tetras() == 0) return;

    auto t_idx1 = (Triangulation::TetraIndex)ui->flipTetra1->value();
    auto t_idx2 = (Triangulation::TetraIndex)ui->flipTetra2->value();
    auto t_idx3 = (Triangulation::TetraIndex)ui->flipTetra3->value();
    auto t_idx4 = (Triangulation::TetraIndex)ui->flipTetra4->value();
    if (!(tri.is_valid_tetra(t_idx1) && tri.is_valid_tetra(t_idx2) && tri.is_valid_tetra(t_idx3) && tri.is_valid_tetra(t_idx4))) {
        QMessageBox::warning(this, "Paramètres de l'opération flip invalides", "Un ou plusieurs indices ne sont pas valides.");
        return;
    }
    tri.flip44(t_idx1, t_idx2, t_idx3, t_idx4);
    emit this->tri->dataChanged();
}

void TriangulationParams::walk()
{
    auto& tri = this->tri->tri;
    if (tri.n_tetras() == 0) return;

    auto t = tri.contains(Vec3f(ui->walkX->value(), ui->walkY->value(), ui->walkZ->value()));
    ui->showTetra->setChecked(true);
    ui->showTetraIndex->setValue(t);
    showTri();
}

Mesh::Point TriangulationParams::calcTriCOG(Triangulation::TetraIndex t) const
{
    auto& tri = this->tri->tri;
    assert (tri.n_tetras() > 0);
    auto &vertices = tri.tetra(t).vertices;
    return (tri.vertex(vertices[0]).position + tri.vertex(vertices[1]).position + tri.vertex(vertices[2]).position + tri.vertex(vertices[3]).position) / 4.f;
}

std::array<Mesh::Point, 2> TriangulationParams::calcTriBoundingBox(Triangulation::TetraIndex t) const
{
    auto& tri = this->tri->tri;
    assert (tri.n_tetras() > 0);
    auto &vertices = tri.tetra(t).vertices;
    return {tri.vertex(vertices[0]).position.cwiseMin(tri.vertex(vertices[1]).position).cwiseMin(tri.vertex(vertices[2]).position).cwiseMin(tri.vertex(vertices[3]).position),
            tri.vertex(vertices[0]).position.cwiseMax(tri.vertex(vertices[1]).position).cwiseMax(tri.vertex(vertices[2]).position).cwiseMax(tri.vertex(vertices[3]).position)};
}

float TriangulationParams::calcTriRadius(Triangulation::TetraIndex t, const Mesh::Point &cog) const
{
    auto box = calcTriBoundingBox(t);
    return std::max((cog - box[0]).norm(), (cog - box[1]).norm());
}

Vec3f TriangulationParams::calcCOG() const
{
    auto& tri = this->tri->tri;
    assert (tri.n_tetras() > 0);
    return (tri.vertex(0).position + tri.vertex(1).position + tri.vertex(2).position + tri.vertex(3).position) / 4.f;
}

std::array<Vec3f, 2> TriangulationParams::calcBoundingBox() const
{
    auto& tri = this->tri->tri;
    assert (tri.n_tetras() > 0);
    return {tri.vertex(0).position.cwiseMin(tri.vertex(1).position).cwiseMin(tri.vertex(2).position).cwiseMin(tri.vertex(3).position),
            tri.vertex(0).position.cwiseMax(tri.vertex(1).position).cwiseMax(tri.vertex(2).position).cwiseMax(tri.vertex(3).position)};
}

float TriangulationParams::calcRadius(const Vec3f &cog) const
{
    auto box = calcBoundingBox();
    return std::max((cog - box[0]).norm(), (cog - box[1]).norm());
}

void TriangulationParams::pointFromWalk()
{
    auto &m = view->matrix(pointDI);
    m.setToIdentity();
    m.translate(ui->walkX->value(), ui->walkY->value(), ui->walkZ->value());
    view->update();
}

void TriangulationParams::pointFromFlip()
{
    auto &m = view->matrix(pointDI);
    m.setToIdentity();
    m.translate(ui->newVertexX->value(), ui->newVertexY->value(), ui->newVertexZ->value());
    view->update();
}

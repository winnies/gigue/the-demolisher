#include "object.h"
#include <QWidget>
#include "datastream.h"
#include "randompointsparams.h"
#include "meshparams.h"

Object::Object(Type type, QObject *parent)
    : QObject(parent)
    , type(type)
    , params(nullptr)
    , m_isShown(true)
{
}

Object::~Object()
{
    if (params) {
        delete params.data();
    }
}

bool Object::isShown() const
{
    return m_isShown;
}

void Object::setIsShown(bool _isShown)
{
    if (m_isShown != _isShown) {
        m_isShown = _isShown;
        emit isShownChanged(m_isShown);
    }
}

QDataStream &operator<<(QDataStream &s, const ObjectPtr &o)
{
    s << o->type << o->name << o->isShown();// << o->widget->objectName();
    o->write(s);
    s << *o->params;
    return s;
}

QDataStream &operator>>(QDataStream &s, ObjectPtr &o)
{
    Object::Type type;
    s >> type;
    switch (type) {
    case Object::Type::Points:
        o.reset(new PointsObject);
        break;
    case Object::Type::Mesh:
        o.reset(new MeshObject);
        break;
    case Object::Type::Triangulation:
        o.reset(new TriangulationObject);
        break;
    default:
        o.reset();
        assert(false);
        return s;
    }
    QString widgetName;
    s >> o->name >> o->m_isShown;// >> widgetName;
    o->read(s);
    o->params = Params::read(s, o.get());
    emit o->dataChanged();

    return s;
}

PointsObject::PointsObject(Object *parent)
    : Object(Type::Points, parent)
{
}

void PointsObject::write(QDataStream &s)
{
    s << points;
}

void PointsObject::read(QDataStream &s)
{
    s >> points;
}

MeshObject::MeshObject(Object *parent)
    : Object(Type::Mesh, parent)
{
}

void MeshObject::write(QDataStream &s)
{
    s << fileName;
}

void MeshObject::read(QDataStream &s)
{
    s >> fileName;
    if (!fileName.isEmpty()) {
        mesh.read(fileName.toStdString());
    }
}

TriangulationObject::TriangulationObject(Object * parent)
    : Object(Type::Triangulation, parent)
{}

void TriangulationObject::write(QDataStream &s)
{
    s << tri;
}

void TriangulationObject::read(QDataStream &s)
{
    s >> tri;
}

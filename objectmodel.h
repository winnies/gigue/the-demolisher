#ifndef OBJECTMODEL_H
#define OBJECTMODEL_H

#include <QAbstractListModel>
#include <QSortFilterProxyModel>
#include "object.h"

/*!
 * \brief Un modèle Qt d'objets.
 */
class ObjectModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ObjectModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    const QString &getName(int index) const;
    void setName(int index, const QString& name);

    const Object &get(int index) const;
    Object &get(int index);

    void insert(ObjectPtr object);
    void remove(int index);
    void clear();

private:
    std::vector<ObjectPtr> m_objects;

    friend QDataStream &operator<<(QDataStream &, const ObjectModel &);
    friend QDataStream &operator>>(QDataStream &, ObjectModel &);
};

class ObjectTypeFilterModel : public QSortFilterProxyModel
{
public:
    ObjectTypeFilterModel(Object::Type wantedType, QObject * parent = nullptr);

    Object::Type wantedType;

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &) const override;;
    bool filterAcceptsColumn(int, const QModelIndex &) const override;;
};


#endif // OBJECTMODEL_H

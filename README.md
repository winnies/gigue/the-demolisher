# The Demolisher

## Dépendances

Le programme à été testé avec *QT 5.15.1* et le compilateur *g++ 10.2.0*.
Les packages doxygen et graphviz sont nécessaires pour générer la documentation.

### Utiliser la version précompilée d'OpenMesh

Configuration à ajouter à cmake pour linux :
```
-DOPENMESH_LIBRARY_DIR=OpenMesh/inc;OpenMesh/liblinux;
```

## Documentation

Pour la générer, se positionner dans le répertoire de *build* et taper la commande *make doc* (*make open_doc* l'ouvrir automatiquement ensuite). Le fichier généré est disponible dans le répertoire de *build* ici : *html/index.xhtml*.

### Intégration avec Qt Creator

Il est possible d'accéder à la documentation directement depuis Qt Creator. Pour ce faire il faut d'abord avoir généré la documentation en aillant l’outil *qhelpgenerator* installé. Cela va créer le fichier *the-demolisher.qch* dans *build/html* qu'il faudra ajouter dans Qt Creator en utilisant *Tools > Options > Help > Documentation > Add* comme expliqué [ici](https://doc.qt.io/qtcreator/creator-help.html#adding-external-documentation).

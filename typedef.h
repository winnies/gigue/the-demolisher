#ifndef TYPEDEF_H
#define TYPEDEF_H

#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Geometry>

typedef Eigen::Vector3f Vec3f;
typedef Eigen::Vector4f Vec4f;
typedef Eigen::Matrix3f Mat3f;

#endif // TYPEDEF_H

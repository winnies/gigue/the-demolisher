#ifndef MYOPENGLWIDGET_H
#define MYOPENGLWIDGET_H

#include <QObject>
#include <QWidget>
#include <QKeyEvent>
#include <QTimer>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLDebugMessage>
#include <QOpenGLVertexArrayObject>
#include <unordered_map>
#include "typedef.h"

const float TRACKBALL_RADIUS = 0.6f;
const bool OPENGL_DEBUG      = false;

/*!
 * \brief L'identifiant d'un objet affiché.
 */
using DrawId = size_t;

/*!
 * \brief Éléments d'un objet pouvant être affichés.
 */
enum class DrawMode {
    Point = 1 << 0,
    Line = 1 << 1,
    Triangle = 1 << 2,
};

inline DrawMode operator|(DrawMode a, DrawMode b)
{
    return static_cast<DrawMode>(static_cast<int>(a) | static_cast<int>(b));
}

inline DrawMode operator&(DrawMode a, DrawMode b)
{
    return static_cast<DrawMode>(static_cast<int>(a) & static_cast<int>(b));
}

/*!
 * \brief Widget permettant d'afficher plusieurs objets.
 *
 * Les objets sont des lignes (sommets et arêtes) ou des maillages (sommets, arêtes et faces).
 *
 */
class myOpenGLWidget : public QOpenGLWidget,
			   protected QOpenGLFunctions
{
	Q_OBJECT

    enum class DrawKind {
        Points,
        Mesh
    };

    struct Draw {
        DrawKind kind;
        DrawMode mode;
        GLsizei vertexCount;
        GLsizei elementsLinesCount;
        GLsizei elementsTrianglesCount;
        QOpenGLBuffer vboVertexArray;
        QOpenGLBuffer vboVertexArrayFlat;
        QOpenGLBuffer vboElementsLines;
        QOpenGLBuffer vboElementsTriangles;
        QOpenGLVertexArrayObject* vao = nullptr;
        QOpenGLVertexArrayObject* vaoFlat = nullptr;

        QOpenGLBuffer vboPoints;
        QOpenGLBuffer vboLines;
        QOpenGLBuffer vboTriangles;

        QOpenGLVertexArrayObject* vaoPoints = nullptr;
        QOpenGLVertexArrayObject* vaoLines = nullptr;
        QOpenGLVertexArrayObject* vaoTriangles = nullptr;

        QMatrix4x4 matrix;
        bool isShown = true;
        bool ligthing = false;
        bool flatLighting = true;
        bool useFaceColor = true;

        void tearGLObjects();
    };

public:

    explicit myOpenGLWidget(QWidget *parent = nullptr);
    ~myOpenGLWidget() override;

    /*!
     * \brief Dessine une ligne.
     * \param points les sommets composant la ligne
     * \param color la couleur des sommets
     * \param id identifiant souhaité
     * \return l'identifiant de la ligne
     */
    DrawId drawPoints(const std::vector<Vec3f>& points, const std::vector<QRgb> &colors, DrawId id = 0);

    /*!
     * \brief Dessine un maillage.
     *
     * Le maillage doit posséder les couleurs et normales aux sommets et les normales aux faces.
     * Un maillage triangulaire est préféré, mais pas obligatoire.
     *
     * \param mesh maillage OpenMesh
     * \param id identifiant souhaité
     * \return l'identifiant du maillage
     */
    template<typename Mesh>
    DrawId drawMesh(const Mesh& mesh, DrawId id = 0);

    /*!
     * \brief Renvoie la matrice d'un objet.
     * \param id l'identifiant de l'objet
     * \return la matrice de l'objet
     */
    QMatrix4x4& matrix(DrawId id);
    const QMatrix4x4& matrix(DrawId id) const;

    /*!
     * \brief Renvoie les éléments affichés d'un objet.
     * \param id l'identifiant de l'objet
     * \return les éléments affichés de l'objet
     */
    DrawMode &drawMode(DrawId id);
    DrawMode drawMode(DrawId id) const;

    /*!
     * \brief Indique si un objet est affiché.
     * \param id l'identifiant de l'objet
     * \return vrai si l'objet est affiché
     */
    bool &isShown(DrawId id);
    bool isShown(DrawId id) const;

    /*!
     * \brief Indique si on utilise l'éclairage sur un objet.
     *
     * Il s'agit d'un éclairage avec les normales aux sommets.
     *
     * \param id l'identifiant de l'objet
     * \return vrai si l'éclairage est utilisé sur l'objet
     */
    bool &ligthing(DrawId id);
    bool ligthing(DrawId id) const;

    /*!
     * \brief Indique si on utilise l'éclairage plat sur un objet.
     *
     * C'est un éclairage avec les normales aux faces.
     *
     * \param id l'identifiant de l'objet
     * \return vrai si l'éclairage est utilisé sur l'objet
     */
    bool &flatLigthing(DrawId id);
    bool flatLigthing(DrawId id) const;

    bool &useFaceColor(DrawId id);
    bool useFaceColor(DrawId id) const;

    /*!
     * \brief Supprime un objet.
     * \param id l'identifiant de l'objet
     */
    void remove(DrawId id);

    /*!
     * \brief Supprime tous les objets.
     */
    void clear();

    void view_all();
    void set_scene_pos(const QVector3D& _cog, float _radius);

public slots:
    void messageLogged(QOpenGLDebugMessage message);

protected:
	void initializeGL() override;
	void resizeGL(int w, int h) override;
	void paintGL() override;
	void keyPressEvent(QKeyEvent *ev) override;
	void keyReleaseEvent(QKeyEvent *ev) override;
	void mousePressEvent(QMouseEvent *ev) override;
	void mouseReleaseEvent(QMouseEvent *ev) override;
	void mouseMoveEvent(QMouseEvent *ev) override;
    void wheelEvent(QWheelEvent* ev) override;

    // gestion de la vue et de la trackball
    void update_projection_matrix();
    bool map_to_sphere(const QPoint& _point, QVector3D& _result);
    void translate(const QVector3D& _trans);
    void rotate(const QVector3D& _axis, float _angle);

    Draw &getOrNewDraw(DrawId &id, const Draw& defaultDraw);

private:
    float m_radius = 0.5f;
    QVector3D m_center;

    QPoint last_point_2D_;
    QVector3D last_point_3D_;
    bool last_point_ok_;

	QMatrix4x4 m_modelView;
	QMatrix4x4 m_projection;

	QOpenGLShaderProgram *m_program;

    DrawId next_id = 1;
    std::map<DrawId, Draw> m_draws;

    friend QDataStream &operator<<(QDataStream &, const myOpenGLWidget &);
    friend QDataStream &operator>>(QDataStream &, myOpenGLWidget &);
};

template<typename Mesh>
DrawId myOpenGLWidget::drawMesh(const Mesh& mesh, DrawId id) {
    static Draw defaultDraw;
    defaultDraw.mode = DrawMode::Triangle;

    Draw& draw = getOrNewDraw(id, defaultDraw);
    draw.tearGLObjects();
    draw.kind = DrawKind::Mesh;

    makeCurrent();


    QVector<GLfloat> vertexAttrs;
    {
    vertexAttrs.reserve(mesh.n_faces() * 3 * 5 * 3);

    auto pushV = [&](typename Mesh::VertexHandle vh, typename Mesh::FaceHandle fh) {
        const auto& p = mesh.point(vh);
        const auto& cV = mesh.color(vh);
        const auto& cF = mesh.color(fh);
        const auto& nV = mesh.normal(vh);
        const auto& nF = mesh.normal(fh);
        vertexAttrs << p[0]  << p[1]  << p[2]
                    << cV[0] << cV[1] << cV[2]
                    << cF[0] << cF[1] << cF[2]
                    << nV[0] << nV[1] << nV[2]
                    << nF[0] << nF[1] << nF[2];
    };

    std::vector<typename Mesh::VertexHandle> vertices;
    vertices.reserve(10);
    for (auto f_it=mesh.faces_begin(); f_it!=mesh.faces_end(); ++f_it) {
        vertices.clear();

        for (auto v_it=mesh.cfv_ccwiter(*f_it); v_it.is_valid(); ++v_it) {
            vertices.push_back(*v_it);
        }

        for (size_t i = 1; i < vertices.size() - 1; ++i) {
            pushV(vertices[0], *f_it);
            pushV(vertices[i], *f_it);
            pushV(vertices[i+1], *f_it);
        }
    }

    auto vao = new QOpenGLVertexArrayObject;
    vao->create();
    vao->bind();

    QOpenGLBuffer vboArray(QOpenGLBuffer::VertexBuffer);
    vboArray.create();
    vboArray.bind();
    vboArray.allocate(vertexAttrs.constData(), vertexAttrs.count() * sizeof(GLfloat));

    m_program->setAttributeBuffer("posAttr",     GL_FLOAT, 0,                   3, 15 * sizeof(GLfloat));
    m_program->setAttributeBuffer("colAttr",     GL_FLOAT, 3 * sizeof(GLfloat), 3, 15 * sizeof(GLfloat));
    m_program->setAttributeBuffer("colFaceAttr", GL_FLOAT, 6 * sizeof(GLfloat), 3, 15 * sizeof(GLfloat));
    m_program->setAttributeBuffer("norAttr",     GL_FLOAT, 9 * sizeof(GLfloat), 3, 15 * sizeof(GLfloat));
    m_program->setAttributeBuffer("norFaceAttr", GL_FLOAT, 12 * sizeof(GLfloat), 3, 15 * sizeof(GLfloat));

    m_program->enableAttributeArray("posAttr");
    m_program->enableAttributeArray("colAttr");
    m_program->enableAttributeArray("colFaceAttr");
    m_program->enableAttributeArray("norAttr");
    m_program->enableAttributeArray("norFaceAttr");

    vao->release();

    draw.vboTriangles = vboArray;
    draw.vaoTriangles = vao;
    draw.elementsTrianglesCount = mesh.n_faces() * 3;
   }
{
        vertexAttrs.clear();
        vertexAttrs.reserve(mesh.n_edges() * 2 * 2 * 3);

    auto pushV = [&](typename Mesh::VertexHandle vh, typename Mesh::EdgeHandle eh) {
        const auto& p = mesh.point(vh);
        const auto& c = mesh.color(eh);
        vertexAttrs << p[0] << p[1] << p[2]
                    << c[0] << c[1] << c[2];
    };

    for (auto e_it=mesh.edges_begin(); e_it!=mesh.edges_end(); ++e_it) {
        auto vh1 = mesh.to_vertex_handle(mesh.halfedge_handle(*e_it, 0));
        auto vh2 = mesh.to_vertex_handle(mesh.halfedge_handle(*e_it, 1));
        pushV(vh1, *e_it);
        pushV(vh2, *e_it);
    }

    auto vao = new QOpenGLVertexArrayObject;
    vao->create();
    vao->bind();

    QOpenGLBuffer vboArray(QOpenGLBuffer::VertexBuffer);
    vboArray.create();
    vboArray.bind();
    vboArray.allocate(vertexAttrs.constData(), vertexAttrs.count() * sizeof(GLfloat));

    m_program->setAttributeBuffer("posAttr", GL_FLOAT, 0,                   3, 6 * sizeof(GLfloat));
    m_program->setAttributeBuffer("colAttr", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));

    m_program->enableAttributeArray("posAttr");
    m_program->enableAttributeArray("colAttr");

    vao->release();

    draw.vboLines = vboArray;
    draw.vaoLines = vao;
    draw.elementsLinesCount = mesh.n_edges() * 2;
    }
    {
        vertexAttrs.clear();
        vertexAttrs.reserve(mesh.n_vertices() * 2 * 3);

    for (auto v_it=mesh.vertices_begin(); v_it!=mesh.vertices_end(); ++v_it) {
        const auto& p = mesh.point(*v_it);
        const auto& c = mesh.color(*v_it);
        vertexAttrs << p[0] << p[1] << p[2]
                    << c[0] << c[1] << c[2];
    }

    auto vao = new QOpenGLVertexArrayObject;
    vao->create();
    vao->bind();

    QOpenGLBuffer vboArray(QOpenGLBuffer::VertexBuffer);
    vboArray.create();
    vboArray.bind();
    vboArray.allocate(vertexAttrs.constData(), vertexAttrs.count() * sizeof(GLfloat));

    m_program->setAttributeBuffer("posAttr", GL_FLOAT, 0,                   3, 6 * sizeof(GLfloat));
    m_program->setAttributeBuffer("colAttr", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));

    m_program->enableAttributeArray("posAttr");
    m_program->enableAttributeArray("colAttr");

    vao->release();

    draw.vboPoints = vboArray;
    draw.vaoPoints = vao;
    draw.vertexCount = mesh.n_vertices();
    }

    doneCurrent();


    return id;
}


#endif // MYOPENGLWIDGET_H

#ifndef DATASTREAM_H
#define DATASTREAM_H

#include <QDataStream>
#include <OpenMesh/Core/Geometry/VectorT.hh>
#include "typedef.h"

inline QDataStream &operator<<(QDataStream &s, const size_t& v) {
    return s << quintptr(v);
}

inline QDataStream &operator>>(QDataStream &s, size_t& v) {
    quintptr tmp;
    s >> tmp;
    v = tmp;
    return s;
}

template <typename T>
QDataStream &operator<<(QDataStream &s, const std::vector<T> &v) {
    s << (quintptr)v.size();
    for (const auto& e : v) {
        s << e;
    }
    return s;
}

template <typename T>
QDataStream &operator>>(QDataStream &s, std::vector<T> &v) {
    quintptr size;
    s >> size;
    v.clear();
    v.reserve(size);
    for (size_t i = 0; i < size; ++i) {
        T e;
        s >> e;
        v.push_back(std::move(e));
    }
    return s;
}

template <typename T, size_t SIZE>
QDataStream &operator<<(QDataStream &s, const std::array<T, SIZE> &a) {
    for (const auto& e : a) {
        s << e;
    }
    return s;
}

template <typename T, size_t SIZE>
QDataStream &operator>>(QDataStream &s, std::array<T, SIZE> &a) {
    for (auto& e : a) {
        s >> e;
    }
    return s;
}

template<typename Scalar, int DIM>
QDataStream &operator<<(QDataStream &s, const OpenMesh::VectorT<Scalar, DIM> &v) {
    for (const auto& e : v) {
        s << e;
    }
    return s;
}

template<typename Scalar, int DIM>
QDataStream &operator>>(QDataStream &s, OpenMesh::VectorT<Scalar, DIM> &v) {
    for (auto& e : v) {
        s >> e;
    }
    return s;
}

inline QDataStream &operator<<(QDataStream &s, const Vec3f &v) {
    return s << v[0] << v[1] << v[2];
}

inline QDataStream &operator>>(QDataStream &s, Vec3f &v) {
    return s >> v[0] >> v[1] >> v[2];
}

#endif // DATASTREAM_H

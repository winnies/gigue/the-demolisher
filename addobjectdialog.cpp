#include "addobjectdialog.h"
#include "ui_addobjectdialog.h"

AddObjectDialog::AddObjectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddObjectDialog)
{
    ui->setupUi(this);

    connect(ui->objects, &QListWidget::doubleClicked, this, &AddObjectDialog::accept);
    connect(ui->cancel, &QPushButton::clicked, this, &AddObjectDialog::reject);
    connect(ui->add, &QPushButton::clicked, this, &AddObjectDialog::accept);

    ui->objects->setCurrentRow(0);
}

AddObjectDialog::~AddObjectDialog()
{
    delete ui;
}

QString AddObjectDialog::getObject() const
{
    return ui->objects->currentItem()->data(Qt::DisplayRole).toString();
}

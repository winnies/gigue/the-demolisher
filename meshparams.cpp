#include "meshparams.h"
#include "ui_meshparams.h"

#include <QFileDialog>
#include <QMessageBox>

MeshParams::MeshParams(myOpenGLWidget* view, MeshObject* mesh, QWidget *parent) :
    Params(Type::Mesh, parent),
    ui(new Ui::MeshParams),
    view(view),
    mesh(mesh)
{
    ui->setupUi(this);
    connect(mesh, &MeshObject::dataChanged, this, &MeshParams::loadMesh);
    connect(mesh, &MeshObject::isShownChanged, this, &MeshParams::updateView);
    connect(ui->flat, &QPushButton::clicked, this, &MeshParams::flatMode);
    connect(ui->smooth, &QPushButton::clicked, this, &MeshParams::smoothMode);
    connect(ui->wired, &QPushButton::clicked, this, &MeshParams::wiredMode);
    connect(ui->showVertices, &QCheckBox::clicked, this, &MeshParams::updateView);
    connect(ui->showEdges, &QCheckBox::clicked, this, &MeshParams::updateView);
    connect(ui->showFaces, &QCheckBox::clicked, this, &MeshParams::updateView);
    connect(ui->useFaceColor, &QCheckBox::clicked, this, &MeshParams::updateView);
    connect(ui->useFaceNormal, &QCheckBox::clicked, this, &MeshParams::updateView);
    connect(ui->lighting, &QCheckBox::clicked, this, &MeshParams::updateView);

    loadMesh();
    flatMode();
}

MeshParams::~MeshParams()
{
    delete ui;

    if (meshDI > 0) {
        view->remove(meshDI);
        view->update();
    }
}

void MeshParams::centerObject()
{
    switch (mesh->mesh.n_vertices()) {
    case 0:
        view->set_scene_pos({0, 0, 0}, 1.f);
        break;
    case 1: {
        auto cog = mesh->mesh.point(*mesh->mesh.vertices_begin());
        view->set_scene_pos({cog[0], cog[1], cog[2]}, 1.f);
    } break;
    default: {
        auto cog = mesh->mesh.calc_barycenter();
        auto box = mesh->mesh.calc_safe_box();
        view->set_scene_pos({cog[0], cog[1], cog[2]}, std::max((cog - box.min).norm(), (cog - box.max).norm()));
    } break;
    }

    view->update();
}

void MeshParams::write(QDataStream &s) const
{
    for (auto cb : {ui->showVertices, ui->showEdges, ui->showFaces, ui->lighting, ui->useFaceNormal, ui->useFaceColor}) {
        s << cb->isChecked();
    }
}

void MeshParams::read(QDataStream &s)
{
    bool tmp;
    for (auto cb : {ui->showVertices, ui->showEdges, ui->showFaces, ui->lighting, ui->useFaceNormal, ui->useFaceColor}) {
        s >> tmp;
        cb->setChecked(tmp);
    }
    updateView();
}

void MeshParams::loadMesh()
{
    ui->fileName->setText(mesh->fileName.isEmpty() ? tr("Pas de fichier ouvert") : mesh->fileName);
    meshDI = view->drawMesh<Mesh>(mesh->mesh, meshDI);
    view->update();
}

void MeshParams::updateView()
{
    if (meshDI == 0) {
        return;
    }

    DrawMode drawMode = (DrawMode)0;
    if (ui->showVertices->isChecked()) {
        drawMode = drawMode | DrawMode::Point;
    }
    if (ui->showEdges->isChecked()) {
        drawMode = drawMode | DrawMode::Line;
    }
    if (ui->showFaces->isChecked()) {
        drawMode = drawMode | DrawMode::Triangle;
    }
    view->drawMode(meshDI) = drawMode;
    view->ligthing(meshDI) = ui->lighting->isChecked();
    view->flatLigthing(meshDI) = ui->useFaceNormal->isChecked();
    view->useFaceColor(meshDI) = ui->useFaceColor->isChecked();

    view->isShown(meshDI) = mesh->isShown();
    view->update();
}

void MeshParams::flatMode()
{
    ui->showVertices->setChecked(false);
    ui->showEdges->setChecked(true);
    ui->showFaces->setChecked(true);
    ui->useFaceColor->setChecked(true);
    ui->useFaceNormal->setChecked(true);
    ui->lighting->setChecked(false);
    updateView();
}

void MeshParams::smoothMode()
{
    ui->showVertices->setChecked(false);
    ui->showEdges->setChecked(false);
    ui->showFaces->setChecked(true);
    ui->useFaceColor->setChecked(true);
    ui->useFaceNormal->setChecked(false);
    ui->lighting->setChecked(true);
    updateView();
}

void MeshParams::wiredMode()
{
    ui->showVertices->setChecked(false);
    ui->showEdges->setChecked(true);
    ui->showFaces->setChecked(false);
    updateView();
}

void MeshParams::on_saveAs_clicked()
{
    auto suggestedName = mesh->fileName.isEmpty() ? mesh->name + ".obj" : mesh->fileName;
    auto fileName = QFileDialog::getSaveFileName(this, tr("Enregistrement du maillage"), suggestedName, QString::fromStdString(OpenMesh::IO::IOManager().qt_read_filters()));
    if (fileName.isEmpty()) {
        return;
    }

    if (!mesh->mesh.write(fileName.toStdString())) {
        QMessageBox::critical(this, tr("Enregistrement du maillage"), tr("Une erreur est survenue lors de l'enregistrement de %1").arg(fileName));
    }

    mesh->fileName = fileName;
    emit mesh->dataChanged();
}

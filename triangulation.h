#ifndef TRIANGULATION_H
#define TRIANGULATION_H

#include <array>
#include <vector>
#include "mesh.h"
#include <optional>
#include <tuple>
class QDataStream;

/*!
 * \brief Représente une tétraédrisation via un ensemble de \ref Tetrahedron.
 *
 * ### Notes sur l'ajout et la suppression de tétraèdres
 *
 * L'ajout d'un sommet en dehors d'un tétraèdre existant est un comportement indéfini.
 * La modifiction de la triangulation ne peut se faire qu'à partir des opérations flip.
 * Il faut faire très attention à l'utilisation à postériori des indices des tétraèdres donnés en paramètre à ces opérations.
 * En effet les tétraèdres pointés par ces indices seront considérés comme indéfinis
 * et les utiliser pourait entraîner un comportement indéfini (faire planter l'appli au mieux,
 * au pire endommager la structure de donnée et produire des résultats aberrant).
 *
 * ### Itérer sur tous les tétraèdres :
 *
 *     for (auto tetra_it = tri.tetras_begin(); tetra_it.is_valid(); ++tetra_it) {
 *         const Tetrahedron& tetra = *tetra_it;
 *     }
 *
 * ### Convertir en maillage OpenMesh
 *  Voir into_mesh().
 */
class Triangulation
{
public:
    typedef size_t TetraIndex; //!< Indice d'un tétraèdre. Il est invalide si = 0 (commence à 1).
    typedef size_t VertexIndex; //!< Indice d'un sommet. Pas d'indice invalide possible (commence à 0).

    /*!
     * \brief Le sommet d'un ou plusieurs tétraèdres
     */
    struct Vertex {
        Vec3f position; //!< La position du sommet.
        TetraIndex tetra; //!< Un tétraèdre adjacent.

        Vertex() : tetra(0) {}
        Vertex(const Vec3f& p) : position(p), tetra(0) {}

        friend QDataStream &operator<<(QDataStream &, const Vertex &);
        friend QDataStream &operator>>(QDataStream &, Vertex &);
    };

    /*!
     * \brief Un tétraèdre
     *
     * Il composé de quatre sommets et quatre tétraèdres voisins.
     *  - Ses sommets sont représentés par des indices sur les sommets de \ref Triangulation.
     *  - Ses tétraèdres voisins sont représentés par des indices sur les tétraèdres de \ref Triangulation.
     *
     * \image html tetra.png "Représentation visuelle" width=200 height=200
     *
     * ### Les faces
     *
     * La définition des faces est donnée par #faces, elle est toujours la même.
     * Les sommets sont dans l'ordre anti-horraire, avec la normale pointant vers l'extérieur du tétraèdre.
     *
     * ### Les différents indices
     *
     * \ref InternalVertexIndex est un indice qui se réfère à #vertices.
     * Il ne faut pas le confondre avec \ref VertexIndex qui est un indice sur les sommets de \ref Triangulation.
     *
     * \ref FaceIndex représente une des quatre faces du tétraèdre.
     *
     */
    struct Tetrahedron {
        typedef int8_t FaceIndex; //!< Indice d'une face. Défini de 0 à 3, il est invalide si = -1.
        typedef int8_t InternalVertexIndex; //!< Indice dans #vertices. Défini de 0 à 3, il est invalide si = -1.
        typedef int8_t EdgeIndex;

        static const InternalVertexIndex faces[4][3]; //!< Pour chaque face donne l'indice des trois sommets dans l'ordre anti-horaire
        static const InternalVertexIndex vertex_opposite_to_face[4]; //!< Pour chaque face donne l'indice du sommet opposé
        static const InternalVertexIndex edges[6][2];

        std::array<VertexIndex, 4> vertices; //!< Les sommets qui composent le tétraèdre (dans le référenciel de la triangulation)
        std::array<TetraIndex, 4> neighbours; //!< Pour chaque face donne le tétraèdre voisin

        /*!
         * \brief Retourne les sommets d'une face du tétraèdre
         * \param f
         * \return
         */
        std::array<VertexIndex, 3> face_vertices(FaceIndex f) const {
            return {vertices[faces[f][0]],
                    vertices[faces[f][1]],
                    vertices[faces[f][2]]};
        }

        /*!
         * \brief Retourne la face partagée avec un tétraèdre voisin
         * \param t
         * \return
         */
        FaceIndex shared_face(Triangulation::TetraIndex t) const {
            for (FaceIndex f = 0; f < 4; ++f) {
                if (neighbours[f] == t) {
                    return f;
                }
            }
            return -1;
        }

        /*!
         * \brief Retourne le sommet opposé à une face
         * \param f
         * \return
         */
        VertexIndex opposite_vertex(FaceIndex f) const {
            return vertices[vertex_opposite_to_face[f]];
        }

        std::array<VertexIndex, 2> edge(EdgeIndex e) const {
            return {vertices[edges[e][0]], vertices[edges[e][1]]};
        }

        /*!
         * \brief Retourne vrai si les deux faces sont confondues
         * \param f1
         * \param f2
         * \return
         */
        bool face_equal(const std::array<VertexIndex, 3>& f1, const std::array<VertexIndex, 3>& f2) const {
            for (short i = 0; i < 3; ++i) {
                if (f1[i] == f2[0] &&
                    f1[(i+1)%3] == f2[1] &&
                    f1[(i+2)%3] == f2[2]) {
                    return true;
                }
            }
            return false;
        }

        /*!
         * \brief Permet de trouver une face à partir de ses 3 sommets
         * \param v1
         * \param v2
         * \param v3
         * \return
         */
        FaceIndex find_face_with(VertexIndex v1, VertexIndex v2, VertexIndex v3) const {
            for (FaceIndex f = 0; f < 4; ++f) {
                if (face_equal(face_vertices(f), {v1, v2, v3})) {
                    return f;
                }
            }
            return -1;
        }

        std::optional<VertexIndex> find_third_vertex(const std::array<VertexIndex, 3>& f, VertexIndex v1, VertexIndex v2) const {
            for (short i = 0; i < 3; ++i) {
                if (f[i] == v1 &&
                    f[(i+1)%3] == v2) {
                    return f[(i+2)%3];
                }
            }
            return {};
        }

        std::optional<std::tuple<VertexIndex, FaceIndex>> find_third_vertex(VertexIndex v1, VertexIndex v2) const {
            for (FaceIndex f = 0; f < 4; ++f) {
                if (auto v3 = find_third_vertex(face_vertices(f), v1, v2)) {
                    return std::make_tuple(*v3, f);
                }
            }
            return {};
        }

        /*!
         * \brief Retourne l'index local d'un sommet dans un tétrahèdre
         * à partir de son index global.
         * \param v
         * \return
         */
        InternalVertexIndex get_internal_index(VertexIndex v) const
        {
            InternalVertexIndex result = -1;
            for(int i = 0; i < 4; i++) {
                if(vertices[i] == v){
                    result = i;
                    break;
                }
            }
            return result;
        }

        /*!
         * \brief Retourne la face opposée à un sommet dans un tétrahèdre
         * \param v
         * \return
         */
        FaceIndex opposite_face(VertexIndex v) const
        {
            auto iv = get_internal_index(v);
            FaceIndex result = -1;
            for(int i = 0; i < 4; i++) {
                if(iv == vertex_opposite_to_face[i]){
                    result = i;
                    break;
                }
            }
            return result;
        }

        friend QDataStream &operator<<(QDataStream &, const Tetrahedron &);
        friend QDataStream &operator>>(QDataStream &, Tetrahedron &);
    };

    /*!
     * \brief Itérateur sur les tétraèdres d'une triangulation
     */
    class TetraIterator {
        friend Triangulation;

        const Triangulation* tri;
        TetraIndex current_tetra;

        TetraIterator(const Triangulation* tri, TetraIndex current_tetra)
            : tri(tri), current_tetra(current_tetra) {}
     public:
        const Tetrahedron &operator*() {
            return tri->tetras[current_tetra];
        }
        const Tetrahedron *operator->() {
            return &(operator*());
        }
        TetraIndex index() const {
            return current_tetra;
        }
        TetraIterator &operator++() {
            if (current_tetra < tri->tetras.size()) {
                do {
                    current_tetra += 1;
                } while (current_tetra < tri->tetras.size()
                     && !tri->is_valid_tetra(current_tetra));
            }
            return *this;
        }
        TetraIterator operator++(int) {
            TetraIterator tmp(*this);
            ++(*this);
            return tmp;
        }
        TetraIterator &operator+=(size_t n) {
            for (size_t i = 0; i < n; ++i) {
                ++(*this);
            }
            return *this;
        }
        TetraIterator operator+(size_t n) {
            TetraIterator tmp(*this);
            (*this) += n;
            return tmp;
        }

        bool operator==(const TetraIterator &other) const {
            return current_tetra == other.current_tetra;
        }

        bool operator!=(const TetraIterator &other) const {
            return current_tetra != other.current_tetra;
        }

        bool is_valid() const {
            return current_tetra < tri->tetras.size() && tri->is_valid_tetra(current_tetra);
        }
    };

    /*!
     * \brief Construit une tétraédrisation invalide.
     *
     * \warning La tétraédrisation ne contenant aucun tétraèdre, aucune oppération ne doit être effectué.
     */
    Triangulation();

    /*!
     * \brief Construit une tétraédrisation avec un premier tétraèdre décrit par \a vertices.
     *
     * \note Voir la documentation de \ref Tetrahedron pour l'ordre des sommets.
     *
     * \param vertices Le premier tétraèdre
     */
    Triangulation(const std::array<Vec3f, 4>& vertices);

    bool is_valid_tetra(TetraIndex t) const {
        return 0 < t && t < tetras.size() && next_tetra_indexes[t] == 0;
    }

    const Tetrahedron& tetra(TetraIndex t) const {
        assert(is_valid_tetra(t));
        return tetras[t];
    }

    const Vertex& vertex(VertexIndex v) const {
        assert(v < vertices.size());
        return vertices[v];
    }

    /*!
     * \brief Renvoie le nombre de tétraèdres
     * \return
     */
    size_t n_tetras() const {
        return n_valid_tetras;
    }

    /*!
     * \brief Renvoie le nombre de sommets
     * \return
     */
    size_t n_vertices() const {
        return vertices.size();
    }

    TetraIterator tetras_begin() const {
        return ++TetraIterator(this, 0);
    }

    TetraIterator tetras_end() const {
        return TetraIterator(this, tetras.size());
    }

    auto vertices_begin() const {
        return vertices.begin();
    }

    auto vertices_end() const {
        return vertices.end();
    }

    /*!
     * \brief Réalise le flip 1->4.
     *
     * \warning Le nouveau sommet doit être dans le tétraèdre.
     * \warning Cette opération supprime les tétraèdres donnés en paramètre.
     *
     * \param t Le tétraèdre qui contient le nouveau sommet
     * \param p La position du nouveau sommet
     * \return Les nouveaux tétraèdres.
     */
    std::array<TetraIndex, 4> flip14(TetraIndex t, const Vec3f& p);

    /*!
     * \brief Réalise le flip 2->3.
     *
     * \warning Les deux tétrèdres doivent êtres adjacents (face commune).
     * \warning Cette opération supprime les tétraèdres donnés en paramètre.
     *
     * \param t1
     * \param t2
     * \return Les nouveaux tétraèdres.
     */
    std::array<TetraIndex, 3> flip23(TetraIndex t1, TetraIndex t2);

    /*!
     * \brief Réalise le flip 3->2.
     *
     * \warning Les trois tétrèdres doivent êtres adjacents (arête commune).
     * \warning Cette opération supprime les tétraèdres donnés en paramètre.
     *
     * \param t1
     * \param t2
     * \param t3
     * \return Les nouveaux tétraèdres.
     */
    std::array<TetraIndex, 2> flip32(TetraIndex t1, TetraIndex t2, TetraIndex t3);

    /*!
     * \brief Réalise le flip 4->4.
     *
     * \warning Les quatre tétrèdres doivent partager une arête commune.
     * \warning Cette opération supprime les tétraèdres donnés en paramètre.
     *
     * \param t1
     * \param t2
     * \param t3
     * \param t4
     * \return Les nouveaux tétraèdres.
     */
    std::array<TetraIndex, 4> flip44(TetraIndex t1, TetraIndex t2, TetraIndex t3, TetraIndex t4);

    /*!
     * \brief Essaye de trouver un tétraèdre qui contient \a point.
     * \param point
     * \return L'indice du tétraèdre trouvé, 0 sinon.
     */
    TetraIndex contains(const Vec3f& point);

    /*!
     * \brief Retourne tous les tétrahèdres adjacents à un sommet.
     * \param v
     * \return
     */
    std::vector<TetraIndex> neighbour_tetras(VertexIndex v) const;

    void neighbour_tetras(VertexIndex v, std::vector<TetraIndex> &neighbours) const;

    friend QDataStream &operator<<(QDataStream &, const Triangulation &);
    friend QDataStream &operator>>(QDataStream &, Triangulation &);

private:
    void update_neighbour(TetraIndex neighbour, TetraIndex old_tetra, TetraIndex new_tetra);

    TetraIndex new_tetra();
    void remove_tetra(TetraIndex t);

    std::vector<Vertex> vertices;
    std::vector<Tetrahedron> tetras;

    size_t n_valid_tetras;
    TetraIndex next_tetra_index;
    std::vector<TetraIndex> next_tetra_indexes;
};

template<typename MeshT>
/*!
 * \brief Converti la triangulation en maillage OpenMesh.
 *
 * OpenMesh ne pouvant supporter les maillages non-manifold,
 * chaque tétraèdre devient indépendant (les sommets, arêtes et faces ne sont pas partagés).
 *
 * \param tri
 * \param mesh
 */
void into_mesh(const Triangulation &tri, MeshT &mesh) {
    mesh.clean_keep_reservation();
    mesh.reserve(tri.n_tetras() * 4, tri.n_tetras() * 6, tri.n_tetras() * 4);

    for (auto tetra_it = tri.tetras_begin(); tetra_it.is_valid(); ++tetra_it) {
        const auto& vertices = tetra_it->vertices;
        std::array<typename MeshT::VertexHandle, 4> vhs {mesh.add_vertex(tri.vertex(vertices[0]).position),
                                                         mesh.add_vertex(tri.vertex(vertices[1]).position),
                                                         mesh.add_vertex(tri.vertex(vertices[2]).position),
                                                         mesh.add_vertex(tri.vertex(vertices[3]).position)};

        for (size_t f = 0; f < 4; ++f) {
            auto indices = Triangulation::Tetrahedron::faces[f];
            mesh.add_face(vhs[indices[0]], vhs[indices[1]], vhs[indices[2]]);
        }
    }
}


#endif // TRIANGULATION_H

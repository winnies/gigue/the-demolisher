#ifndef PARAMS_H
#define PARAMS_H

#include <QWidget>
class Object;

class Params : public QWidget
{
    Q_OBJECT

public:
    enum class Type {
        RandomPoints,
        Mesh,
        Triangulation
    };

    const Type type;

    Params(Type type, QWidget * parent = nullptr);
    virtual ~Params() {}

    virtual void centerObject() {}

    static Params* read(QDataStream &, Object*);

protected:
    virtual void write(QDataStream&) const {}
    virtual void read(QDataStream&) {}

    friend QDataStream &operator<<(QDataStream &, const Params &);
};

#endif // PARAMS_H

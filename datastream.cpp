#include "datastream.h"
#include "triangulation.h"

QDataStream &operator<<(QDataStream &s, const Triangulation &t) {
    return s << t.vertices << t.tetras << t.n_valid_tetras << t.next_tetra_index << t.next_tetra_indexes;
}

QDataStream &operator>>(QDataStream &s, Triangulation &t) {
    return s >> t.vertices >> t.tetras >> t.n_valid_tetras >> t.next_tetra_index >> t.next_tetra_indexes;
}

QDataStream &operator<<(QDataStream &s, const Triangulation::Tetrahedron &t) {
    return s << t.vertices << t.neighbours;
}

QDataStream &operator>>(QDataStream &s, Triangulation::Tetrahedron &t) {
    return s >> t.vertices >> t.neighbours;
}

QDataStream &operator<<(QDataStream &s, const Triangulation::Vertex &v) {
    return s << v.position << v.tetra;
}

QDataStream &operator>>(QDataStream &s, Triangulation::Vertex &v) {
    return s >> v.position >> v.tetra;
}

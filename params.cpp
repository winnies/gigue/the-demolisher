#include "params.h"
#include "object.h"
#include "randompointsparams.h"
#include "meshparams.h"
#include "triangulationparams.h"

Params::Params(Params::Type type, QWidget *parent)
    : QWidget(parent)
    , type(type)
{
}

myOpenGLWidget* objectReadView = nullptr;

Params *Params::read(QDataStream &s, Object *o)
{
    Type type;
    s >> type;
    Params *p;
    switch (type) {
    case Type::RandomPoints:
        p = new RandomPointsParams(objectReadView, dynamic_cast<PointsObject*>(o));
        break;
    case Type::Mesh:
        p = new MeshParams(objectReadView, dynamic_cast<MeshObject*>(o));
        break;
    case Type::Triangulation:
        p = new TriangulationParams(objectReadView, dynamic_cast<TriangulationObject*>(o));
        break;
    }
    p->read(s);
    return p;
}

QDataStream &operator<<(QDataStream &s, const Params &p) {
    s << p.type;
    p.write(s);
    return s;
}

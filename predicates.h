#ifndef PREDICATES_H
#define PREDICATES_H

#include "typedef.h"

namespace predicates {

void init();

/*!
 * \brief orient3D. On se base sur le fait que le plan a, b et c est vu "du dessus" avec a, b et c dans le sens
 * inverse des aiguilles d'une montre.
 * \param a
 * \param b
 * \param c
 * \param d
 * \return > 0 : d est derrière le plan abc , < 0 : d est devant le plan abc, == 0 : d est sur le plan abc
 */
float orient3D(const Vec3f& a, const Vec3f& b, const Vec3f& c, const Vec3f& d);

/*!
 * \brief orient3DFast. On se base sur le fait que le plan a, b et c est vu "du dessus" avec a, b et c dans le sens
 * inverse des aiguilles d'une montre.
 * \param a
 * \param b
 * \param c
 * \param d
 * \return > 0 : d est derrière le plan abc , < 0 : d est devant le plan abc, == 0 : d est sur le plan abc
 */
float orient3DFast(const Vec3f& a, const Vec3f& b, const Vec3f& c, const Vec3f& d);


/*!
 * \brief inSphere3D
 * \param a
 * \param b
 * \param c
 * \param d
 * \param e
 * \return > 0 : à l'intérieur, < 0 : à l'extérieur, == 0 : sur la sphère
 */
float inSphere3D(const Vec3f& a, const Vec3f& b, const Vec3f& c, const Vec3f& d, const Vec3f& e);

}

#endif // PREDICATES_H

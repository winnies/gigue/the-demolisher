#ifndef RANDOMPOINTSPARAMS_H
#define RANDOMPOINTSPARAMS_H

#include "myopenglwidget.h"
#include "object.h"
#include "params.h"

namespace Ui {
class RandomPointsParams;
}

class RandomPointsParams : public Params
{
    Q_OBJECT

public:
    explicit RandomPointsParams(myOpenGLWidget* view, PointsObject* randomPoints, QWidget *parent = nullptr);
    ~RandomPointsParams();

    void centerObject();

protected:
    void write(QDataStream&) const;
    void read(QDataStream&);

private:
    void createPoints();
    void loadPoints();
    void updateView();

private:
    Ui::RandomPointsParams *ui;
    myOpenGLWidget* view;
    PointsObject* randomPoints;

    DrawId pointsDI = 0;

    Vec3f cog = Vec3f::Constant(0);
    float radius = 1.f;
};

#endif // RANDOMPOINTSPARAMS_H

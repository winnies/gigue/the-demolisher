#ifndef TRIANGULATIONPARAMS_H
#define TRIANGULATIONPARAMS_H

#include "params.h"
#include "myopenglwidget.h"
#include "object.h"

namespace Ui {
class TriangulationParams;
}

class TriangulationParams : public Params
{
    Q_OBJECT

public:
    explicit TriangulationParams(myOpenGLWidget* view, TriangulationObject* tri, QWidget *parent = nullptr);
    ~TriangulationParams();

    void centerObject();

protected:
    void write(QDataStream&) const;
    void read(QDataStream&);

private slots:
    void loadTri();
    void showTri();
    void updateView();

    void createTri();
    void flip14();
    void flip23();
    void flip32();
    void flip44();
    void walk();

    Mesh::Point calcTriCOG(Triangulation::TetraIndex t) const;
    std::array<Mesh::Point, 2> calcTriBoundingBox(Triangulation::TetraIndex t) const;
    float calcTriRadius(Triangulation::TetraIndex t, const Mesh::Point& cog) const;

    Vec3f calcCOG() const;
    std::array<Vec3f, 2> calcBoundingBox() const;
    float calcRadius(const Vec3f& cog) const;

    void pointFromWalk();
    void pointFromFlip();

private:
    Ui::TriangulationParams *ui;
    myOpenGLWidget* view;
    TriangulationObject* tri;

    DrawId triDI = 0, pointDI = 0;

    Vec3f triCOG = Vec3f::Constant(0);
    float triRadius = 1.f;
};

#endif // TRIANGULATIONPARAMS_H

#include "algo.h"

#include <QVector3D>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/tags.h>

#include <CGAL/Polygon_mesh_processing/connected_components.h>
#include <CGAL/Polygon_mesh_processing/corefinement.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel  K;
typedef K::Point_3                                Point_3;
typedef CGAL::Surface_mesh<Point_3>               Surface_mesh;
namespace PMP = CGAL::Polygon_mesh_processing;

struct Plane {
    Vec3f point, normal;
};

typedef std::vector<Plane> Cell;
typedef std::vector<Cell> Cells;

void convertSurfaceMeshToMesh(const Surface_mesh &source, Mesh &target)
{
    typedef Point_3 CGALVertex;
    typedef Surface_mesh::Vertex_index vertex_descriptor;
    typedef Vec3f MeshVertex;
    typedef OpenMesh::VertexHandle MeshVertexHandle;
    typedef vertex_descriptor PointKey;

    target.clean_keep_reservation();

    std::map<PointKey, MeshVertexHandle> points_map;
    PointKey key;
    MeshVertexHandle p_h;

    CGALVertex v;
    MeshVertex p;
    std::vector<MeshVertexHandle> face_vhandles;

    for (auto f : source.faces())
    {
        for (auto vd : source.vertices_around_face(source.halfedge(f)))
        {
            key = vd;
            if (points_map.count(key) == 0)
            {
                auto temp = source.point(vd);
                p = MeshVertex(temp[0], temp[1], temp[2]);
                p_h = target.add_vertex(p);
                points_map[key] = p_h;
                target.set_color(p_h, {1, 0, 0});
            }
            else
            {
                p_h = points_map[key];
            }
            face_vhandles.push_back(p_h);
        }
        auto f_h = target.add_face(face_vhandles);
        target.set_color(f_h, {0.58, 0.58, 0.58});
        face_vhandles.clear();
    }
}

void convertMeshToSurfaceMesh(const Mesh &source, Surface_mesh &target)
{
    typedef Point_3 CGALVertex;
    typedef Surface_mesh::Vertex_index vertex_descriptor;
    typedef Mesh::VertexHandle PointKey;

    target.clear();

    std::map<PointKey, vertex_descriptor> points_map;
    PointKey key;
    vertex_descriptor p_h;

    CGALVertex v;
    Point_3 p;
    std::vector<vertex_descriptor> face_vhandles;


    for (Mesh::FaceIter f=source.faces_begin(); f!=source.faces_end(); ++f){
        for (Mesh::FaceVertexIter vd=source.cfv_iter(*f); vd.is_valid(); ++vd)
        {
            key = *vd;
            if (points_map.count(key) == 0)
            {
                auto temp = source.point(*vd);
                p = Point_3(temp[0], temp[1], temp[2]);
                p_h = target.add_vertex(p);
                points_map[key] = p_h;
            }
            else
            {
                p_h = points_map[key];
            }
            face_vhandles.push_back(p_h);
        }
        target.add_face(face_vhandles);
        face_vhandles.clear();
    }
}

void algo::intersection(const Mesh &mesh, const Mesh &cells, Mesh &result) {

    Surface_mesh sf_mesh, sf_cells, sf_result;
    convertMeshToSurfaceMesh(mesh, sf_mesh);
    convertMeshToSurfaceMesh(cells, sf_cells);

    std::vector<Surface_mesh> cc_cells;

    PMP::split_connected_components(sf_cells, cc_cells);

    for (auto& cell : cc_cells) {
        Surface_mesh sh_mesh_copy = sf_mesh;
        PMP::corefine_and_compute_intersection(sh_mesh_copy, cell, sf_result);
    }

    convertSurfaceMeshToMesh(sf_result, result);
}

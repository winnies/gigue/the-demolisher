#ifndef ALGO_H
#define ALGO_H

#include "triangulation.h"
#include "mesh.h"

#include <vector>

namespace algo {

/*!
* \brief Calcule un tétraèdre englobant le modèle passé en paramètre
* \param mesh
* \param withBaseMesh TRUE La fonction retourne un tétraèdre de base avec le modèle à l'intérieur / FALSE La fonction retourne un tétraèdre seulement
* \return Retourne un tétraèdre
*/
Mesh incorporateInTetra(const Mesh& mesh, bool withBaseMesh);

Triangulation incorporateInTetraTri(const Mesh& mesh);

void delaunay(const std::vector<Vec3f>& seeds, Triangulation& tri);

void dual(const Triangulation& tri, Mesh& result);

void intersection(const Mesh& mesh, const Mesh& cells, Mesh& result);

}

#endif // ALGO_H

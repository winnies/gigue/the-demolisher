varying lowp vec4 col;
varying highp vec3 nor;
uniform bool lighting;
uniform vec3 lightDir;

void main() {
    if (lighting) {
        vec3 light = normalize(lightDir);
        vec3 nor3 = normalize(nor);
        float cosTheta = clamp(abs(dot(nor3, light)), 0.5, 1.0);
        vec3 lightColor = vec3(1.0, 1.0, 1.0);
        gl_FragColor = vec4(lightColor * cosTheta, 1.0) * col;
    } else {
        gl_FragColor = col;
    }
}


#include "randompointsparams.h"
#include "ui_randompointsparams.h"
#include <random>

RandomPointsParams::RandomPointsParams(myOpenGLWidget* view, PointsObject* randomPoints, QWidget *parent) :
    Params(Type::RandomPoints, parent),
    ui(new Ui::RandomPointsParams),
    view(view),
    randomPoints(randomPoints)
{
    ui->setupUi(this);
    connect(randomPoints, &PointsObject::dataChanged, this, &RandomPointsParams::loadPoints);
    connect(randomPoints, &PointsObject::isShownChanged, this, &RandomPointsParams::updateView);
    connect(ui->distribution, QOverload<int>::of(&QComboBox::currentIndexChanged), ui->stackedWidget, &QStackedWidget::setCurrentIndex);
    connect(ui->create, &QPushButton::clicked, this, &RandomPointsParams::createPoints);
    ui->distribution->setCurrentIndex(0);
    ui->stackedWidget->setCurrentIndex(0);

    loadPoints();
    updateView();
}

RandomPointsParams::~RandomPointsParams()
{
    delete ui;

    if (pointsDI > 0) {
        view->remove(pointsDI);
        view->update();
    }
}

void RandomPointsParams::centerObject()
{
    view->set_scene_pos({cog[0], cog[1], cog[2]}, radius);
    view->update();
}

void RandomPointsParams::write(QDataStream &s) const
{
   s << ui->nbPoints->value() << ui->distribution->currentIndex()
     << ui->minX->value() << ui->maxX->value()
     << ui->minY->value() << ui->maxY->value()
     << ui->minZ->value() << ui->maxZ->value()
     << ui->meanX->value() << ui->devX->value()
     << ui->meanY->value() << ui->devY->value()
     << ui->meanZ->value() << ui->devZ->value();
}

void RandomPointsParams::read(QDataStream &s)
{
     {
    int tmp;
    s >> tmp; ui->nbPoints->setValue(tmp);
    s >> tmp; ui->distribution->setCurrentIndex(tmp);}{double tmp;
    for (auto * spinbox : {ui->minX, ui->maxX
         , ui->minY , ui->maxY
         , ui->minZ , ui->maxZ
         , ui->meanX , ui->devX
         , ui->meanY , ui->devY
         , ui->meanZ , ui->devZ}) {
        s >> tmp; spinbox->setValue(tmp);
    }}
    updateView();
}

void RandomPointsParams::createPoints()
{
    auto& points = randomPoints->points;
    auto nbPoints = ui->nbPoints->value();
    assert(0 <= nbPoints);

    points.clear();
    points.reserve(nbPoints);

    std::random_device rd;
    std::mt19937 gen(rd());
    switch (ui->distribution->currentIndex()) {
    case 0: {
        std::uniform_real_distribution<float> disX(ui->minX->value(), ui->maxX->value()),
                disY(ui->minY->value(), ui->maxY->value()),
                disZ(ui->minZ->value(), ui->maxZ->value());
        for (int i = 0; i < nbPoints; ++i) {
            points.push_back({disX(gen), disY(gen), disZ(gen)});
        }
    }break;
    case 1: {
        std::normal_distribution<float> disX(ui->meanX->value(), ui->devX->value()),
                disY(ui->meanY->value(), ui->devY->value()),
                disZ(ui->meanZ->value(), ui->devZ->value());
        for (int i = 0; i < nbPoints; ++i) {
            points.push_back({disX(gen), disY(gen), disZ(gen)});
        }
    }break;
    default:
        return;
    }

    loadPoints();
}

void RandomPointsParams::loadPoints()
{
    cog = Vec3f::Constant(0.f);
    radius = 0;
    for (const auto& p : randomPoints->points) {
        cog += p;
        radius = std::max(radius, std::max(std::abs(p[0]), std::max(std::abs(p[1]), std::abs(p[2]))));
    }
    cog /= randomPoints->points.size();

    std::vector<QRgb> color{qRgb(0, 0, 0)};
    pointsDI = view->drawPoints(randomPoints->points, color, pointsDI);
    view->update();
}

void RandomPointsParams::updateView()
{
    if (pointsDI > 0) {
        view->isShown(pointsDI) = randomPoints->isShown();
        view->update();
    }
}

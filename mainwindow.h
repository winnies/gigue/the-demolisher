#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "objectmodel.h"
#include "voronoidialog.h"

namespace Ui {
class MainWindow;
}

/*!
 * \brief La fenêtre principale de l'application.
 */
class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

protected:
    void closeEvent(QCloseEvent*);

private:
    void addObject();
    MeshObject* addObjectMaillage (const QString& fileName);
    void removeCurrentObject();
    void renameCurrentObject();
    void showChanged(bool checked);
    void showCurrentObject();
    void centerCurrentObject();
    void loadSession();
    void saveSession();

private slots:
    void dragEnterEvent(QDragEnterEvent* event) override;
    void dropEvent(QDropEvent* event) override;

    void on_actionVoronoi_triggered();

    void on_actionRecentrer_l_origine_triggered();

private:
	Ui::MainWindow *ui;

    quintptr nextObjectId = 1;
    ObjectModel* m_objectModel;

    VoronoiDialog *voronoiDiag;
};

#endif // MAINWINDOW_H
